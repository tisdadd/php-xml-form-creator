# PHP XML Form Creator

The repository for the PHP XML Form Creator to live separate from the [website](https://phpxmlformcreator.adsittech.com/), as if someone is wanting to use this and update it will be easier to fork as a starting point.

## At a glance

The PHP form Creator is used to easily create and process XML based forms for the lazy developer. Please see the [manual](https://phpxmlformcreator.adsittech.com/manual.php) to learn more.

## License

MIT Licensed, feel free to go wild.

## Disclaimer

This was created originally in 2012, and while it has worked well for me, there are many other projects available that may accomplish the same thing and be maintened by others. Please research, or play on the interactives page to decide if this is right for you.

In addition, this was created when the author had only dial-up and a large stack of school-books. If the author was to recreate today it would be made differently with an easier installer and namespacing. However, he has other projects that he is working on with his wife so just wanted to make this available for others who may be interested.