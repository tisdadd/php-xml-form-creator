function changeDivType(divId)
{
	div = document.getElementById(divId);
	divClass = div.getAttribute("class");
	arrow = document.getElementById(divId + "-arrow");
	if(divClass == "drop-down-closed")
	{
		div.setAttribute("class", "drop-down-open");
		arrow.innerHTML = "&uarr;";
	}
	else
	{
		div.setAttribute("class", "drop-down-closed");
		arrow.innerHTML = "&darr;";
	}
		
}