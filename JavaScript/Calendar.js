function dayChange(dateName, day, displaytype, label) {
	document.getElementById(dateName + "_Day").value = day;
	fixDisplay(dateName, displaytype, label);
}

function monthChange(dateName, month, displaytype, label) {
	document.getElementById(dateName + "_Month").value = month;
	fixDisplay(dateName, displaytype, label);
}

function yearChange(dateName, year, displaytype, label) {
	document.getElementById(dateName + "_Year").value = year;

	if (document.getElementById(dateName + "_Month").value == 2
			&& document.getElementById(dateName + "_Day").value == 29
			&& year % 4 != 0) {
		document.getElementById(dateName + "_Day").value = 28;
	}
	fixDisplay(dateName, displaytype, label);
}

function nextMonth(dateName, displaytype, label) {
	if (typeof dateName == "undefined" || dateName == null) {
		dateName = "";
	}
	var month = Number(document.getElementById(dateName + "_Month").value) + 1;
	var year = Number(document.getElementById(dateName + "_Year").value);
	if ((month % 12) == 1) {
		year++;
		document.getElementById(dateName + "_Year").value = year;
		month = 1;
	}

	document.getElementById(dateName + "_Month").value = month;
	fixDisplay(dateName, displaytype, label);
}

function lastMonth(dateName, displaytype, label) {
	if (typeof dateName == "undefined" || dateName == null) {
		dateName = "";
	}
	var month = Number(document.getElementById(dateName + "_Month").value) - 1;
	var year = Number(document.getElementById(dateName + "_Year").value);
	if ((month % 12) == 0) {
		year--;
		document.getElementById(dateName + "_Year").value = year;
		month = 12;
	}

	document.getElementById(dateName + "_Month").value = month;
	fixDisplay(dateName, displaytype, label);
}

function fixDisplay(dateName, displaytype, label) {
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		XMLHTTP = new XMLHttpRequest();
	} else {// code for IE6, IE5
		XMLHTTP = new ActiveXObject("Microsoft.XMLHTTP");
	}
	XMLHTTP.onreadystatechange = function() {
		if (XMLHTTP.readyState == 4 && XMLHTTP.status == 200) {
			document.getElementById(dateName + "_display").innerHTML = XMLHTTP.responseText;
		}
	};

	if (displaytype == null) {
		displaytype = "calendar";
	}
	date = document.getElementById(dateName + "_Month").value + "/"
			+ document.getElementById(dateName + "_Day").value + "/"
			+ document.getElementById(dateName + "_Year").value;
	minute = document.getElementById(dateName + "_Minute");
	hour = document.getElementById(dateName + "_Hour");
	ampm = document.getElementById(dateName + "_AMPM");

	if (minute != null && hour != null && ampm != null) 
	{
		date += " " + ("00" + hour.value).substr(-2) + ":" + ("00" + minute.value).substr(-2) + " " + ampm.value;
	}
	
	date = encodeURIComponent(date);
	label = encodeURIComponent(label);

	XMLHTTP.open("GET", "PHP/CalendarAjax.php?functionName=dateChange&name="
			+ dateName + "&displaytype=" + displaytype + "&date=" + date + "&label=" + label, true);
	XMLHTTP.send();
}