<?php

/*
 * Copyright Michael Adsit 2012.
 */

include_once('Input.php');

class Label extends Input
{
	public function __construct($xml, $style, $method, $submitted)
	{
		parent::__construct($xml, $style, $method, $submitted);
	}

	public function getHTML()
	{
		$style = $this->getStyle();
		$toReturn = "";
		$toReturn .= $this->generateErrorString();
		$label = $this->label;

		if($style == "table")
		{
			$toReturn .="<tr><td colspan=\"2\">";
		}
			
		$genericElements = $this->getGenericElementString();
		if(isset($genericElements) && $genericElements != "")
		{
			$toReturn .= "<span " . $genericElements . ">" . $label . "</span>";
		}
		else
		{
			$toReturn .= $label;
		}

		if($style == "table")
		{
			$toReturn .= "</td></tr>";
		}
		else if ($style == "row")
		{
			$toReturn .= "<br />";
		}

		return $toReturn;
	}
}
?>