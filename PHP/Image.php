<?php
/*
 * Copyright Michael Adsit 2012.
 */

include_once('Input.php');

class Image extends Input
{
	private $source;

	public function __construct($xml, $style, $method, $submitted)
	{
		parent::__construct($xml, $style, $method, $submitted);
		$this->generateUniqueElements($xml);
	}

	public function getHTML()
	{
		$source = $this->getSource();
		$label = $this->getLabel();
		$style = $this->getStyle();
		$toReturn = "";
		$toReturn .= $this->generateErrorString();
			
		if($style == "table")
		{
			$toReturn .="<tr><td align=\"left\"><input type=\"image\" alt=\"" . $label . "\" " . trim($this->getGenericElementString());
		}
		else
		{
			$toReturn .= trim("<input type=\"image\" alt=\"" . $label . "\" " . trim($this->getGenericElementString()));
		}

		if(isset($source))
		{
			$toReturn .= " src=\"" . $source . "\"";
		}

		$toReturn .= " />";

		if($style == "table")
		{
			$toReturn .= "</td></tr>";
		}
		else if ($style == "row")
		{
			$toReturn .= "<br />";
		}

		return $toReturn;
	}

	private function generateUniqueElements($xml)
	{
		$this->setSource($xml->source);
	}

	private function setSource($source)
	{
		if(!isset($source))
		{
			$source = "";
		}
		$this->source = $source;
	}

	private function getSource()
	{
		if(!isset($this->source))
		{
			$this->setSource("");
		}
			
		return $this->source;
	}

	public function getX()
	{
		if($this->getSubmitted())
		{
			if(strtolower($this->getMethod() == "post" && isset($_POST[$this->getName() . "_x"])))
			{
				return $_POST[$this->getName() . "_x"];
			}
			else if(strtolower($this->getMethod() == "get" && isset($_GET[$this->getName() . "_x"])))
			{
				return $_GET[$this->getName() . "_x"];
			}
			else
			{
				return -1;
			}
		}
	}

	public function getY()
	{
		if($this->getSubmitted())
		{
			if(strtolower($this->getMethod() == "post" && isset($_POST[$this->getName() . "_y"])))
			{
				return $_POST[$this->getName() . "_y"];
			}
			else if(strtolower($this->getMethod() == "get" && isset($_GET[$this->getName() . "_y"])))
			{
				return $_GET[$this->getName() . "_y"];
			}
			else
			{
				return -1;
			}
		}
	}
}
?>