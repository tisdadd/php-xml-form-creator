<?php

class Node
{
	private $level;
	private $parentID;
	private $id;
	private $data;
	private $children;

	public function __construct($level = 0, $parentID = 0, $id = 0, $data = null, $children = array())
	{
		$this->setData($data);
		$this->setLevel($level);
		$this->setID($id);
		$this->setParentID($parentID);
		$this->setChildren($children);
	}

	public function getLevel()
	{
		return $this->level;
	}

	public function setLevel($level)
	{
		if(!(isset($level) && strval(intval($level))===strval($level)))
		{
			$level = 0;
		}
		$this->level = $level;
	}

	public function setParentID($parentID)
	{
		if(!(isset($parentID) && strval(intval($parentID))===strval($parentID)))
		{
			$parentID = 0;
		}
		$this->parentID = $parentID;
	}

	public function getParentID()
	{
		return $this->parentID;
	}

	public function setID($id)
	{
		if(!(isset($id) && strval(intval($id))===strval($id)))
		{
			$id = 0;
		}
		$this->id = $id;
	}

	public function getID()
	{
		return $this->id;
	}

	public function setData($data)
	{
		$this->data = $data;
	}

	public function getData()
	{
		return $this->data;
	}

	public function setChildren($children)
	{
		if(!is_array($children))
		{
			$children = array();
		}
			
		$this->children = $children;
	}

	public function getChildren()
	{
		return $this->children;
	}

	public function addChild(Node $child)
	{
		$this->children[] = $child;
	}
}
?>