<?php

/*
 * Copyright Michael Adsit 2012.
 */

include_once('TextBox.php');

class Password extends TextBox
{
	public function getHTML()
	{
		$cols = $this->getCols();
		$maxLength = $this->getMaxLength();
		$label = $this->getLabel();
		$style = $this->getStyle();
		$toReturn = "";
		$toReturn .= $this->generateErrorString();
			
		if($style == "table")
		{
			$toReturn .="<tr><td align=\"right\">" . trim($label) . "</td><td align=\"left\">" . "<input type=\"password\" " . trim($this->getGenericElementString());
		}
		else
		{
			$toReturn .= trim($label . " <input type=\"password\" " . trim($this->getGenericElementString()));
		}

		if(isset($cols) && $cols != "")
		{
			$toReturn .= " size=\"" . $this->getCols() . "\"";
		}

		if(isset($maxLength) && $maxLength != "")
		{
			$toReturn .= " maxlength=\"" . $this->getMaxLength() . "\"";
		}

		$toReturn .= " />";

		if($style == "table")
		{
			$toReturn .= "</td></tr>";
		}
		else if ($style == "row")
		{
			$toReturn .= "<br />";
		}

		return $toReturn;
	}
}
?>