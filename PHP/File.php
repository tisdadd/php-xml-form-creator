<?php
/*
 * Copyright Michael Adsit 2012.
 */

include_once('Input.php');
include_once('FileSave.php');

class File extends Input
{
	private $accept;
	private $multiple;
	private $required;
	private $save;


	public function __construct($xml, $style, $method, $submitted)
	{
		parent::__construct($xml, $style, $method, $submitted);
		$this->generateUniqueElements($xml);
		if($this->getName() != "")
		{
			$this->setName($this->getName());
		}
	}

	protected function generateUniqueElements()
	{
		$xml = $this->xml;
		if(isset($xml) && $xml instanceof SimpleXMLElement)
		{
			$this->setAccept($xml->accept);
			$this->setMultiple($xml->multiple);
			$this->setRequired($xml->required);
		}
	}

	public function getHTML()
	{
		$accept = $this->getAccept();
		$multiple = $this->getMultiple();
		$required = $this->getRequired();
		$label = $this->getLabel();
		$style = $this->getStyle();
		$toReturn = "";
		$toReturn .= $this->generateErrorString();

		$save = $this->getSave();
		if(isset($save) && $save instanceof FileSave)
		{
			$saveErrors= $save->getErrorString();
			if(!(strpos($saveErrors, "Generic file error #4.") === false) && !$this->getRequired())
			{
				if($style == "table")
				{
					$toReturn .="<tr><td colspan=\"2\">You chose not to upload a file.</tr></td>";
				}
				else
				{
					$toReturn .= "You chose not to upload a file.";
				}
			}
			else if($saveErrors <> "")
			{
				if($style == "table")
				{
					$toReturn .="<tr><td colspan=\"2\">" . $saveErrors . "</tr></td>";
				}
				else
				{
					$toReturn .= $saveErrors;
				}
			}
		}

		if($style == "table")
		{
			$toReturn .="<tr><td align=\"right\">" . trim($label) . "</td><td align=\"left\">" . "<input type=\"file\" " . trim($this->getGenericElementString());
		}
		else
		{
			$toReturn .= trim($label . " <input type=\"file\" " .  trim($this->getGenericElementString()));
		}

		if(isset($accept) && is_array($accept) && count($accept) > 0)
		{
			$toReturn .= " accept=\"". implode(",", $accept) . "\"";
		}
			
		if(isset($multiple) && $multiple)
		{
			$toReturn .= " multiple=\"multiple\"";
		}
			
		if(isset($required) && $required)
		{
			$toReturn .= " required=\"required\"";
		}

		$toReturn .= " />";

		if($style == "table")
		{
			$toReturn .= "</td></tr>";
		}
		else if ($style == "row")
		{
			$toReturn .= "<br />";
		}

		return $toReturn;
	}

	private function addAccept($toAdd)
	{
		if(!isset($this->accept) || !is_array($this->accept))
		{
			$this->accept = array();
		}
		if(isset($toAdd) && (string) $toAdd != "")
		{
			$this->accept[] = (string) $toAdd;
		}
	}

	private function setAccept($accept)
	{
		if(!isset($accept))
		{
			$accept = array();
		}
		if(is_array($accept))
		{
			$this->accept = $accept;
		}
		else if($accept instanceof SimpleXMLElement)
		{
			if(isset($accept->filetype))
			{
				foreach($accept->filetype AS $toAdd)
				{
					$this->addAccept($toAdd);
				}
			}
		}
		else
		{
			$this->accept = array();
		}
	}

	public function getAccept()
	{
		if(!isset($this->accept))
		{
			$this->setAccept(array());
		}
		return $this->accept;
	}

	private function setMultiple($multiple)
	{
		if(!isset($multiple))
		{
			$multiple = false;
		}
			
		$this->multiple = (bool) $multiple;
	}

	private function getMultiple()
	{
		if(!isset($this->multiple))
		{
			$this->setMultiple(false);
		}
		return $this->multiple;
	}

	private function setRequired($required)
	{
		if(!isset($required))
		{
			$required = false;
		}

		$this->required = (bool) ((string) $required);
	}

	private function getRequired()
	{
		if(!isset($this->required))
		{
			$this->setRequired(false);
		}
		return $this->required;
	}
	
	public function getSave()
	{
		if(!isset($this->save) || !($this->save instanceof FileSave))
		{
			return false;
		}
			
		return $this->save;
	}

	public function setSave($save)
	{
		if(isset($save) && $save instanceof FileSave)
		{
			$this->save = $save;
		}
	}
	
	public function getValue()
	{
		$toReturn = array();
		
		$currentSave = $this->getSave();
		if(isset($currentSave) && $currentSave instanceof FileSave)
		{
			$fileInfo = $currentSave->getFileInfo();
			foreach($fileInfo['name'] as $info)
			{
				$toReturn[] = $info;
			}
		}
		
		return $toReturn;
	}

	public function setName($name)
	{
		$name = (string) $name;
		if($this->getMultiple() && $name <> "" && substr($name, -2) <> "[]")
		{
			$name = $name . "[]";
		}
		$this->name = $name;
	}
}
?>