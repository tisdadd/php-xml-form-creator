<?php
/*
 * Copyright Michael Adsit 2012.
 */
class CustomError
{
	private $condition;
	private $message;
	private $toCheck;
	private $type; // the condition type

	public function __construct($xml, $toCheck)
	{
		$this->setType($xml->type);
		$this->setCondition($xml->condition);
		$this->setMessage($xml->message);
		$this->setToCheck($toCheck);
	}

	/*
	 * If a type is normal, then there must be a condition with it, else there is just a regex;
	 * If this error is for a checkbox, then there could be the condition that at least a certain number must be checked.
	 */
	public function validTypes()
	{
		return array("normal", "regex", "numchecked", "extra");
	}

	/*
	 * Conditions: "blank" - item is not allowed to be blank
	 * 			   "numeric" - item must be numeric
	 * 			   "alpha" - item must use only alphabetic characters
	 * 			   "alphanumeric" - item must only use alphanumeric characters
	 * 			   "float" - item must be a float
	 * 			   "int" - item must be an int
	 * 			   "double" - item must be a double
	 * 			   "long" - item must be a long
	 * 			   "date" - make sure item is a valid date/time
	 */
	public function validConditions()
	{
		return array("blank", "numeric", "alpha", "alphanumeric", "float", "int", "double", "long", "date");
	}

	public function getErrorMessage()
	{
		if(!$this->checkCondition())
		{
			return $this->getMessage();
		}
	}

	private function checkCondition()
	{
		$toCheck = $this->getToCheck();
			
		if(!isset($toCheck))
		{
			return false;
		}
		$type = $this->getType();
		$condition = $this->getCondition();
		if($type == "regex")
		{
			if(preg_match($condition, $toCheck))
			{
				return true;
			}
			return false;
		}
			
		if($type == "extra")
		{
			
			if(isset($_POST['error'][$condition]) || isset($_GET['error'][$condition]))
			{
				if(isset($_POST['error']['wrongPass']['message']))
				{
					$this->setMessage($_POST['error'][$condition]['message']);
				}
				else if(isset($_GET['error'][$condition]['message']))
				{
					$this->setMessage($_GET['error'][$condition]['message']);
				}
				return false;
			}
		}
		
			
		if($type == "numchecked")
		{
			$numChecked = count($toCheck);
			if($numChecked >= $condition['min'] && ($numChecked <= $condition['max'] || $condition['max'] == -1))
			{
				return true;
			}
			return false;
		}
			
		if(
		($type == "normal") &&
		(($condition == "blank" && $toCheck == "")
		|| ($condition == "numeric" && !is_numeric($toCheck))
		|| ($condition == "alpha" && !preg_match("#^[a-z]*$#i", $toCheck))
		|| ($condition == "alphanumeric" && !preg_match("#^[a-z0-9]*$#i", $toCheck))
		|| ($condition == "float" && !(strval(floatval($toCheck)) === strval($toCheck)))
		|| ($condition == "double" && !(strval(doubleval($toCheck)) === strval($toCheck)))
		|| (in_array($condition, array("int", "long")) && !(strval(intval($toCheck)) === strval($toCheck))))
		)
		{ // a condition was not met
			return false;
		}
		if($type == "normal" && $condition=="date")
		{
			if(!($toCheck instanceof DateTime))
			{
				$toCheck = strtotime($toCheck);
				if($toCheck == -1 || $toCheck === false)
				{
					return false;
				}
			}
		}
			
		return true;
	}

	private function setCondition($condition)
	{
		if($this->getType() == "numchecked")
		{
			if($condition instanceof SimpleXMLElement)
			{
				if(isset($condition->min))
				{
					$min = (int) ((string) $condition->min);
				}
					
				if(isset($condition->max))
				{
					$max = (int) ((string) $condition->max);
				}
			}
			if(!isset($min))
			{
				$min = 0;
			}

			if(!isset($max))
			{
				$max = -1;
			}

			if($min > $max && $max != -1)
			{
				$temp = $min;
				$min = $max;
				$max = $temp;
			}

			$condition = array();
			$condition['min'] = $min;
			$condition['max'] = $max;
			$this->condition = $condition;
		}
		else
		{
			$condition = strtolower((string) $condition);
			$type = $this->getType();
			if(in_array($type, array("regex", "extra")) || in_array($condition, $this->validConditions()))
			{
				$this->condition = strtolower($condition);
			}
			else
			{
				$this->condition = "blank";
			}
		}
	}

	private function getCondition()
	{
		if(!isset($this->condition) || $this->condition == "")
		{
			$this->setCondition("blank");
		}
		return $this->condition;
	}

	private function setType($type)
	{
		$validTypes = $this->validTypes();
		if(in_array($type, $validTypes))
		{
			$this->type = $type;
		}
		else if(isset($validTypes[0]))
		{
			$this->type = $validTypes[0];
		}
		else
		{
			$this->type = "normal";
		}
	}

	private function getType()
	{
		if(!isset($this->type) || $this->type == "")
		{
			$this->setType("normal");
		}
		return strtolower($this->type);
	}

	private function setToCheck($toCheck)
	{
		$this->toCheck = $toCheck;
	}

	private function getToCheck()
	{
		if(!isset($this->toCheck))
		{
			$this->setToCheck("");
		}
		return $this->toCheck;
	}

	private function setMessage($message)
	{
		$this->message = (string) $message;
	}

	private function getMessage()
	{
		return $this->message;
	}
}
?>