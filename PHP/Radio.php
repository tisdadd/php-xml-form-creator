<?php
/*
 * Copyright Michael Adsit 2012.
*/

	include_once('CheckBox.php');
	
	class Radio extends CheckBox
	{
		public function getHTML()
		{
			$checked = $this->getChecked();
			$style = $this->getStyle();
			$toReturn = "";
			$toReturn .= $this->generateErrorString();
		
			if($style == "table")
			{
				$toReturn .="<tr><td align=\"right\"><input type =\"radio\" " . trim($this->getGenericElementString());
			}
			else
			{
				$toReturn .= "<input type =\"radio\" " . trim($this->getGenericElementString());
			}
		
			if(isset($checked) && $checked)
			{
				$toReturn .= " checked=\"checked\"";
			}
			
			$toReturn .= " />";
			
			if($style == "table")
			{
				$toReturn .= "</td><td align=\"left\">";
			}
		
			$toReturn .= trim($this->getLabel());
		
			if($style == "table")
			{
				$toReturn .= "</td></tr>";
			}
			else if ($style == "row")
			{
				$toReturn .= "<br />";
			}
		
			return $toReturn;
		}
	}
?>