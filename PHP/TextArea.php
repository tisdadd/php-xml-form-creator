<?php
/*
 * Copyright Michael Adsit 2012.
 */
include_once('Input.php');

class TextArea extends Input
{
	protected $cols;
	protected $rows;

	public function __construct($xml, $style, $method, $submitted)
	{
		parent::__construct($xml, $style, $method, $submitted);
		$this->generateUniqueElements($xml);
	}

	protected function generateUniqueElements()
	{
		$xml = $this->xml;
		if(isset($xml) && $xml instanceof SimpleXMLElement)
		{
			$this->setCols($xml->cols);
			$this->setRows($xml->rows);
		}
	}

	public function getHTML()
	{
		$cols = $this->getCols();
		$rows = $this->getRows();
		$label = $this->getLabel();
		$labelClass = $this->getLabelClass();
		if($labelClass != "")
		{
			$label = '<span class="' . $labelClass .'">' . $label . '</span>';
		}
		$style = $this->getStyle();
		$toReturn = "";
		$toReturn .= $this->generateErrorString();
			
		if($style == "table")
		{
			$toReturn .="<tr><td align=\"right\">" . trim($label) . "</td><td align=\"left\">" . "<textarea " . trim($this->getGenericElementString());
		}
		else
		{
			$toReturn .= trim($label . "<br /><textarea " . trim($this->getGenericElementString()));
		}
			
		if(isset($cols) && is_numeric($cols))
		{
			$toReturn .= " cols=\"" . $cols . "\"";
		}
			
		if(isset($rows) && is_numeric($rows))
		{
			$toReturn .= " rows=\"" . $rows . "\"";
		}
			
		$toReturn .= ">" . $this->getValue() . "</textarea>";
			
		if($style == "table")
		{
			$toReturn .= "</td></tr>";
		}
		else if ($style == "row")
		{
			$toReturn .= "<br />";
		}
			
		return $toReturn;
	}

	protected function setCols($cols)
	{
		$this->cols = (string) $cols;
	}

	protected function getCols()
	{
		return $this->cols;
	}

	protected function setRows($rows)
	{
		$this->rows = (string) $rows;
	}

	protected function getRows()
	{
		return $this->rows;
	}
}
?>