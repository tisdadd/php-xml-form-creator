<?php
/*
 * Copyright Michael Adsit 2012.
 */
include_once('Input.php');

class DropDown extends Input
{
	protected $elements;

	public function __construct($xml, $style, $method, $submitted)
	{
		parent::__construct($xml, $style, $method, $submitted);
		$this->generateUniqueElements($xml);
	}

	public function getHTML()
	{
		$style = $this->getStyle();
		$toReturn = "";
		$toReturn .= $this->generateErrorString();
		$name = $this->getName();
			
		if($style == "table")
		{
			$toReturn .= "<tr><td>" . $this->getLabel() . "</td><td>";
		}
		else
		{
			$toReturn .= $this->getLabel();
		}
			
		$toReturn .= '<div id="'. $name .'" class="drop-down-closed">
			<span id="' . $name . '-arrow" onclick="changeDivType(\'' . $name . '\')" class="drop-down-arrow">&darr;</span>';
		foreach($this->getElements() as $element)
		{
			$toReturn .= $element->getHTML();
		}
		$toReturn .= '</div>';
			
		if($style == "table")
		{
			$toReturn .= "</td></tr>";
		}
			
		return $toReturn;
	}

	protected function generateUniqueElements()
	{
		$xml = $this->xml;
		if(isset($xml) && $xml instanceof SimpleXMLElement)
		{
			$this->setElements($xml);
		}
	}

	protected function setElements($xml)
	{
		$elements = array();
		if(isset($xml) && $xml instanceof SimpleXMLElement)
		{
			$toCheck = array();
			if($this->getSubmitted())
			{
				if($this->getMethod() == "get")
				{
					if(isset($_GET[$this->getName()]))
					{
						$toCheck = $_GET[$this->getName()];
					}
				}
				else
				{
					if(isset($_POST[$this->getName()]))
					{
						$toCheck = $_POST[$this->getName()];
					}
				}
			}
			foreach($xml->input as $element)
			{
				if($element->type == "checkbox")
				{
					$element = new CheckBox($element, "row", $this->getMethod(), $this->getSubmitted());
					$name = $this->getName();
					if(substr($name, -2) != "[]")
					{
						$name = $name . "[]";
					}
					$element->setName($name);

					if(in_array($element->getValue(), $toCheck))
					{
						$element->setChecked(true);
					}


					$elements[] = $element;
				}
				if($element->type == "radio")
				{
					$element = new Radio($element, "row", $this->getMethod(), $this->getSubmitted());
					$name = $this->getName();
					if(substr($name, -2) != "[]")
					{
						$name = $name . "[]";
					}
					$element->setName($name);

					if(in_array($element->getValue(), $toCheck))
					{
						$element->setChecked(true);
					}


					$elements[] = $element;
				}
			}
		}
		$this->elements = $elements;
	}

	public function getElements()
	{
		if(!isset($this->elements))
		{
			$this->setElements($this->getXML());
		}
		return $this->elements;
	}

	public function getValue()
	{
		$toReturn = array();
		foreach($this->getElements() as $element)
		{
			if($element->getChecked())
			{
				$toReturn[] = $element->getValue();
			}
		}
		return $toReturn;
	}
}
?>