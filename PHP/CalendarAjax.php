<?php
/*
 * Copyright Michael Adsit 2012.
 */
include_once('Calendar.php');
if(array_key_exists("functionName", $_GET) )
{
	$functionName = $_GET["functionName"];
	/* The functions used for ajax calling, which are the only functions that this should call.
	 All of these take no parameters, and can be safely called from javascript.
		*/
	if(isset($functionName) && $functionName == "dateChange")
	{
		call_user_func("dateChange");
	}
}

function dateChange()
{
	$date = urldecode($_GET["date"]);
	$dateCheck = strtotime($date);
	$dateName = "";
	$dateLabel = "";
	$displayType = "calendar";
	if(isset($_GET["name"]))
	{
		$dateName = $_GET["name"];
	}
	if(isset($_GET["displaytype"]))
	{
		$displayType = $_GET["displaytype"];
	}
	if(isset($_GET["label"]))
	{
		$dateLabel = $_GET["label"];
	}
	if($dateCheck === false || $dateCheck == -1)
	{
		return;
	}
	$date = new Calendar(new SimpleXMLElement("<input><label>" . $dateLabel . "</label><type>calendar</type><displaytype>" . $displayType . "</displaytype><name>" . $dateName . "</name><value>" . $date . "</value></input>"), NULL, NULL, NULL);
	echo $date->getHTML();
}

?>