<?php
/*
 * Copyright Michael Adsit 2012.
 */
include_once('TextBox.php');
include_once('CheckBox.php');
include_once('Password.php');
include_once('Select.php');
include_once('Button.php');
include_once('InputButton.php');
include_once('Reset.php');
include_once('Submit.php');
include_once('Hidden.php');
include_once('File.php');
include_once('FileSave.php');
include_once('Radio.php');
include_once('TextArea.php');
include_once('Calendar.php');
include_once('Label.php');
include_once('Image.php');
include_once('DropDown.php');
include_once('UnorderedList.php');

class Form
{
	public static $numForms = 0;
	private $xml;
	private $form;
	private $elements;
	private $method;
	private $action;
	private $style;
	private $class;
	private $name;
	private $submitted;
	private $elementNames;
	private $encodeType; // for use with POST method
	private $phpVariables;

	public function __construct($fromFile, $toParse)
	{
		Form::$numForms++;
		$this->setXML($fromFile, $toParse);
	}

	private function getXMLFromString($string)
	{
		if(!strpos($string, '<?xml version'))
		{
			$string = '<?xml version="1.0" encoding="UTF-8"?>' . utf8_encode($string);
		}
		$this->xml = new SimpleXMLElement($string);
	}

	private function getXMLFromFile($filename)
	{
		$this->xml = new SimpleXMLElement(file_get_contents($filename));
	}

	public function setXML($fromFile, $toParse)
	{
		if($fromFile)
		{
			$this->getXMLFromFile($toParse);
		}
		else
		{
			$this->getXMLFromString($toParse);
		}
			
		$this->generateFullXML();
		$this->generateForm();
	}

	public function isErrored()
	{
		if(!$this->getSubmitted())
		{
			return true;
		}
			
		foreach($this->getElements() as $element)
		{
			if($element->generateErrorString() != "")
			{
				return true;
			}
		}
			
		return false;
	}

	/*
	 * Goal of this function is to make sure that the XML is set to the proper form
	 */
	private function generateFullXML()
	{
		$xml = "<form>" . $this->recursiveXMLExpansion($this->xml) . "</form>";
		$this->xml = new SimpleXMLElement($xml);
	}

	/*
	 * Should be doing recursively
		*/
	private function recursiveXMLExpansion($xml)
	{
		$toReturn = "";
		foreach($xml->attributes() as $attribute=>$value)
		{
			if($attribute == "a")
			{
				$attribute = "action";
			}
			else if($attribute == "c")
			{
				$attribute = "class";
			}
			else if($attribute == "ch")
			{
				$attribute = "checked";
			}
			else if($attribute == "con")
			{
				$attribute = "condition";
			}
			else if($attribute == "d")
			{
				$attribute = "disabled";
			}
			else if($attribute == "e")
			{
				$attribute = "error";
			}
			else if($attribute == "h")
			{
				$attribute = "hidden";
			}
			else if($attribute == "i")
			{
				$attribute = "input";
			}
			else if($attribute == "js")
			{
				$attribute = "javascript";
			}
			else if($attribute == "l")
			{
				$attribute = "label";
			}
			else if($attribute == "m")
			{
				$attribute = "method";
			}
			else if($attribute == "n")
			{
				$attribute = "name";
			}
			else if($attribute == "p")
			{
				$attribute = "php";
			}
			else if($attribute == "l")
			{
				$attribute = "label";
			}
			else if($attribute == "ob")
			{
				$attribute = "onblur";
			}
			else if($attribute == "och")
			{
				$attribute = "onchange";
			}
			else if($attribute == "ocl")
			{
				$attribute = "onclick";
			}
			else if($attribute == "odbl")
			{
				$attribute = "ondblclick";
			}
			else if($attribute == "of")
			{
				$attribute = "onfocus";
			}
			else if($attribute == "okd")
			{
				$attribute = "onkeydown";
			}
			else if($attribute == "okp")
			{
				$attribute = "onkeypress";
			}
			else if($attribute == "oku")
			{
				$attribute = "onkeyup";
			}
			else if($attribute == "omd")
			{
				$attribute = "onmousedown";
			}
			else if($attribute == "omm")
			{
				$attribute = "onmousemove";
			}
			else if($attribute == "omout")
			{
				$attribute = "onmouseout";
			}
			else if($attribute == "omover")
			{
				$attribute = "onmouseover";
			}
			else if($attribute == "omu")
			{
				$attribute = "onmouseup";
			}
			else if($attribute == "os")
			{
				$attribute = "onselect";
			}
			else if($attribute == "m")
			{
				$attribute = "message";
			}
			else if($attribute == "s")
			{
				$attribute = "style";
			}
			else if($attribute == "t")
			{
				$attribute = "tabindex";
			}
			else if($attribute == "v")
			{
				$attribute = "value";
			}

			$toReturn .= "<" . $attribute . ">" .  $value . "</" . $attribute . ">";
		}

		if(count($xml) == 0)
		{
			return $toReturn;
		}

		foreach($xml->children() as $child)
		{
			$name = $child->getName();

			if($name == "a")
			{
				$name = "action";
			}
			else if($name == "c")
			{
				$name = "class";
			}
			else if($name == "ch")
			{
				$name = "checked";
			}
			else if($name == "con")
			{
				$name = "condition";
			}
			else if($name == "d")
			{
				$name = "disabled";
			}
			else if($name == "e")
			{
				$name = "error";
			}
			else if($name == "h")
			{
				$name = "hidden";
			}
			else if($name == "i")
			{
				$name = "input";
			}
			else if($name == "js")
			{
				$name = "javascript";
			}
			else if($name == "l")
			{
				$name = "label";
			}
			else if($name == "m")
			{
				$name = "method";
			}
			else if($name == "n")
			{
				$name = "name";
			}
			else if($name == "p")
			{
				$name = "php";
			}
			else if($name == "l")
			{
				$name = "label";
			}
			else if($name == "ob")
			{
				$name = "onblur";
			}
			else if($name == "och")
			{
				$name = "onchange";
			}
			else if($name == "ocl")
			{
				$name = "onclick";
			}
			else if($name == "odbl")
			{
				$name = "ondblclick";
			}
			else if($name == "of")
			{
				$name = "onfocus";
			}
			else if($name == "okd")
			{
				$name = "onkeydown";
			}
			else if($name == "okp")
			{
				$name = "onkeypress";
			}
			else if($name == "oku")
			{
				$name = "onkeyup";
			}
			else if($name == "omd")
			{
				$name = "onmousedown";
			}
			else if($name == "omm")
			{
				$name = "onmousemove";
			}
			else if($name == "omout")
			{
				$name = "onmouseout";
			}
			else if($name == "omover")
			{
				$name = "onmouseover";
			}
			else if($name == "omu")
			{
				$name = "onmouseup";
			}
			else if($name == "os")
			{
				$name = "onselect";
			}
			else if($name == "m")
			{
				$name = "message";
			}
			else if($name == "s")
			{
				$name = "style";
			}
			else if($name == "t")
			{
				$name = "tabindex";
			}
			else if($name == "v")
			{
				$name = "value";
			}
			$toReturn .= "<" . $name . ">" . $this->recursiveXMLExpansion($child) . (string) $child . "</" . $name . ">";
		}
		return $toReturn;
	}

	private function generateForm()
	{
		$this->elements = array();
		$xml = $this->xml;
		if(isset($xml) && $xml instanceof SimpleXMLElement)
		{
			$this->setMethod($xml->method);
			$this->setAction($xml->action);
			$this->setStyle($xml->style);
			$this->setClass($xml->class);
			$this->setName($xml->name);
			$this->setEncodeType($xml->encodetype);

			$method = $this->getMethod();

			if(strtolower($method) == "post")
			{
				if(isset($_POST[$this->getName() . "_submitted"]))
				{
					$this->setSubmitted(true);
				}
				else
				{
					$this->setSubmitted(false);
				}
			}
			else if(strtolower($method) == "get")
			{
				if(isset($_GET[$this->getName() . "_submitted"]))
				{
					$this->setSubmitted(true);
				}
				else
				{
					$this->setSubmitted(false);
				}
			}

			foreach($xml->input AS $input)
			{
				$this->addElementViaXML($input);
			}

			if($this->getSubmitted())
			{
				$this->generatePHPVariables();
			}
		}
	}

	public function getFormHTML()
	{
		$method = $this->getMethod();
		$action = $this->getAction();
		$style = $this->getStyle();
		$class = $this->getClass();
		$name = $this->getName();
		$encodeType = $this->getEncodeType();
		$toReturn = "<form";
			
		if(isset($method) && $method != "")
		{
			$toReturn .= " name=\"" . $name . "\"";
		}
			
		if(isset($method) && $method != "")
		{
			$toReturn .= " method=\"" . $method . "\"";

			if(isset($encodeType) && $encodeType != "" && strtolower($method) == "post")
			{
				$toReturn .= " enctype=\"" . $encodeType . "\"";
			}
		}
			
		if(isset($action) && $action != "")
		{
			$toReturn .= " action=\"" . $action . "\"";
		}
			
		if(isset($class) && $class != "")
		{
			$toReturn .= " class=\"" . $class . "\"";
		}
			
			
		$toReturn .= ">";
			
		$toReturn .= "<input type=\"hidden\" name=\"" . $name . "_submitted\" value=\"true\" />";
		foreach($this->elements as $element)
		{
			if($element instanceof Hidden)
			{
				$toReturn .= $element->getHTML();
			}
		}
		$inATable = false;
		if($style == "table")
		{
			$toReturn .= "<table>";
			$inATable = true;
		}

		foreach($this->elements as $element)
		{
			if(!($element instanceof Hidden))
			{
				$surroundingDivClass = $element->getSurroundingDivClass();
				$surroundingSpanClass = $element->getSurroundingSpanClass();
				if($surroundingDivClass != "")
				{
					$toReturn .= '<div class="' . $surroundingDivClass . '">';
				}

				if($surroundingSpanClass != "")
				{
					$toReturn .= '<span class="' . $surroundingSpanClass . '">';
				}
					
				if($element instanceof Calendar)
				{
					if($style == "table")
					{
						$toReturn .= "<tr><td colspan=2>";
					}
					$toReturn .= "<div id=\"" . $element->getName() . "_display\">";
				}
					
				$toReturn .= $element->getHTML();
					
				if($element instanceof Calendar)
				{
					if($style == "table")
					{
						$toReturn .= "</td></tr>";
					}
					else
					{
						$toReturn .= "<br />";
					}
					$toReturn .= "</div>";
				}

				if($surroundingSpanClass != "")
				{
					$toReturn .= '</span>';
				}

				if($surroundingDivClass != "")
				{
					$toReturn .= '</div>';
				}

			}
		}
			
		if($style == "table")
		{
			$toReturn .= "</table>";
		}
			
		$toReturn .= "</form>";
		return $toReturn;
	}

	private function setMethod($method)
	{
		if(in_array(strtolower($method), array("get", "post")))
		{
			$this->method = (string) $method;
		}
		else
		{
			$this->method = "post";
		}
	}

	private function getMethod()
	{
		if(!isset($this->method) || $this->method == "")
		{
			$this->setMethod("post");
		}
		return $this->method;
	}

	private function setAction($action)
	{
		$this->action = (string) $action;
	}

	private function getAction()
	{
		return $this->action;
	}

	private function setStyle($style)
	{
		$style = (string) $style;
		if($style == "")
		{
			$this->style = "table";
		}
		else
		{
			$this->style = (string) $style;
		}
	}

	private function getStyle()
	{
		return $this->style;
	}

	private function setClass($class)
	{
		$this->class = (string) $class;
	}

	private function getClass()
	{
		return $this->class;
	}

	private function addElementName($name)
	{
		if(!isset($this->elementNames))
		{
			$this->elementNames = array();
		}
		if(!array_search($name, $this->elementNames))
		{
			$this->elementNames[] = $name;
		}
	}

	private function doesElementNameExist($name)
	{
		if(!isset($this->elementNames))
		{
			$this->elementNames = array();
		}
		if(array_search($name, $this->elementNames) === false)
		{
			return false;
		}
		return true;
	}

	private function addElement($element, $xml)
	{
		if(!isset($this->elements))
		{
			$this->elements = array();
		}
			
		$name = $element->getName();
		$originalName = $name;
			
		if($element instanceof Radio)
		{
			if($name == "")
			{
				$name = "UnnamedRadio";
			}
			if(substr($name, -2) != "[]")
			{
				$name = $name . "[]";
			}
		}
		else if($element instanceof CheckBox)
		{ // need this second, as Radio extends Checkbox
			if($name == "")
			{
				$name = "UnnamedCheckBox";
			}
			if(substr($name, -2) != "[]")
			{
				$name = $name . "[]";
			}
		}
		else if(!$element instanceof Label)
		{
			if($name == "")
			{
				$name = "UnnamedInputElement";
			}
			$i = 0;
			$wasNamed = $name;
			while($this->doesElementNameExist($name))
			{
				$name = $wasNamed . "_" . $i;
				$i++;
			}
		}
			
		$element->setName($name);
			
			
		/*
		 * Checkboxes should have unique values to be useful for getting information from groups of them
		 */
		if($element instanceof CheckBox)
		{
			$value = $element->getValue();
			if(!isset($value) || $value == "")
			{
				$value = "NoneSpecified";
			}
			$originalValue = $value;
			$i = 0;
			while($this->checkIfCheckBoxValueExists($name, $value))
			{
				$value = $originalValue . "_" . $i;
				$i++;
			}
			$element->setValue($value);
		}
			
		if(substr($name, -2) == "[]")
		{
			$name = substr($name, 0, -2);
		}
			
		if($element instanceof File && $this->getSubmitted() && isset($xml) && isset($_FILES[$name]))
		{
			$element->setSave(new FileSave($xml, $element->getAccept(), $_FILES[$name]));
		}

		$method = $this->getMethod();
		$submitted = false;
		if(strtolower($method) == "post")
		{
			if(isset($_POST[$this->getName() . "_submitted"]))
			{
				$submitted = $_POST[$this->getName() . "_submitted"];
			}

			if(isset($_POST[$name]))
			{
				$toCheck = $_POST[$name];
			}
		}
		else if(strtolower($method) == "get")
		{
			if(isset($_GET[$this->getName() . "_submitted"]))
			{
				$submitted = $_GET[$this->getName() . "_submitted"];
			}
			if(isset($_GET[$name]))
			{
				$toCheck = $_GET[$name];
			}
		}
			
		if($submitted)
		{
			if($element instanceof DropDown)
			{
				foreach($element->getElements() as $checkbox)
				{
					if($checkbox instanceof CheckBox)
					{ // in case dropdown is ever extended
						if(!isset($toCheck) || !is_array($toCheck))
						{
							$toCheck = array();
						}
						if(in_array($checkbox->getValue(), $toCheck))
						{
							$checkbox->setChecked(true);
						}
						else
						{
							$checkbox->setChecked(false);
						}
					}
				}
			}
			else if($element instanceof CheckBox)
			{
				if(!isset($toCheck) || !is_array($toCheck))
				{
					$toCheck = array();
				}
				if(in_array($value, $toCheck))
				{
					$element->setChecked(true);
				}
				else
				{
					$element->setChecked(false);
				}
			}
			else if(isset($toCheck))
			{
				$element->setValue($toCheck);
			}
			$element->resetErrors();
		}

		$this->addElementName($name);
		$this->elements[] = $element;
	}

	private function getElements()
	{
		if(!isset($this->elements))
		{
			$this->elements = array();
		}
			
		return $this->elements;
	}

	private function checkIfCheckBoxValueExists($checkBoxName, $value)
	{
		$elements = $this->getElements();
		foreach($elements as $element)
		{
			if($element instanceof CheckBox && $element->getName() == $checkBoxName && $element->getValue() == $value)
			{
				return true;
			}
		}
		return false;
	}

	private function setName($name)
	{
		$name = (string) $name;
		if(!isset($name) || $name == "")
		{
			$name = "Form" . Form::$numForms;
		}
		$this->name = $name;
	}

	private function getName()
	{
		return $this->name;
	}

	private function setSubmitted($submitted)
	{
		if($submitted == false)
		{
			$this->submitted = false;
		}
		else
		{
			$this->submitted = true;
		}
	}

	private function getSubmitted()
	{
		if(!isset($this->submitted))
		{
			$this->setSubmitted(false);
		}
		return $this->submitted;
	}

	private function setEncodeType($encodeType)
	{
		if(!isset($encodeType))
		{
			$encodeType = "";
		}
			
		$this->encodeType = (string) $encodeType;
	}

	private function getEncodeType()
	{
		if(!isset($this->encodeType))
		{
			$this->setEncodeType("");
		}
		return $this->encodeType;
	}

	public function getPHPVariableArray()
	{
		if(!$this->getSubmitted())
		{
			return false;
		}
			
		if(!isset($this->phpVariables))
		{
			$this->generatePHPVariables();
		}
			
		return $this->phpVariables;
	}

	private function generatePHPVariables()
	{
		$elements = $this->getElements();
		$this->phpVariables = array();
		foreach($elements as $element)
		{
			$toAdd = $element->getKeyValueArray();
			if($element instanceof CheckBox)
			{	// basically, need to check if a checkbox group of the same name exists. If it does, then this should be in the same array if any of them have a php variable. Otherwise should use normal PHP variable naming.
				if($toAdd === false)
				{
					$toAdd = $element->getName();
					foreach($elements as $checkbox)
					{
						if($checkbox instanceof CheckBox && !($checkbox === $element) && $checkbox->getName() == $element->getName() && $checkbox->getPHPVariable() != "")
						{
							$toAdd = $checkbox->getPHPVariable();
						}
					}
				}
				else
				{
					$toAdd = key($toAdd);
				}
					
				if(is_string($toAdd) && substr($toAdd, -2) == "[]")
				{
					$toAdd = substr($toAdd, 0, strlen($toAdd) - 2);
				}
					
				if(array_key_exists($toAdd, $this->phpVariables))
				{ // make sure that it is adding to an array...
					$i = 0;
					$originalKey = $toAdd;
					while(isset($this->phpVariables[$toAdd]) && !is_array($this->phpVariables[$toAdd]))
					{
						$toAdd = $originalKey . "_" . $i;
					}
				}
					
				if(!array_key_exists($toAdd, $this->phpVariables))
				{
					$this->phpVariables[$toAdd] = array();
				}
					
				if($element->getChecked())
				{
					$this->phpVariables[$toAdd][] = $element->getValue();
				}
			}
			else
			{
				$keyExists = array_intersect_key($this->phpVariables, $toAdd);
				$keyExists = array_keys($keyExists);
				foreach($keyExists as $key)
				{
					$originalKey = $key;
					$i = 0;
					while(array_key_exists($key, $this->phpVariables))
					{
						$key = $originalKey . "_" . $i;
						$i++;
					}
					$toAdd[$key] = $toAdd[$originalKey];
					unset($toAdd[$originalKey]);
				}
				$this->phpVariables = array_merge($this->phpVariables, $toAdd);
			}
		}
	}

	public function getPHPVariable($key)
	{
		$variables = $this->getPHPVariableArray();
		if($variables === false || !array_key_exists($key, $variables))
		{
			return false;
		}
			
		return $variables[$key];
	}

	public function resetToDefaults()
	{
		foreach($this->getElements() as $element)
		{
			$element->setValue($element->getDefaultValue());
			if($element instanceof CheckBox)
			{
				$element->setChecked($element->getDefaultChecked());
			}
		}
		$this->setSubmitted(false);
	}

	public function addElementViaXML($xml)
	{
		if($xml instanceof SimpleXMLElement)
		{
			$input = $xml;
		}
		else
		{
			$input = $this->recursiveXMLExpansion($xml);
		}
		if(isset($input->type) && $input->type != "")
		{
			unset($element);
			if(strtolower($input->type) == "text")
			{
				$element = new TextBox($input, $this->getStyle(), $this->getMethod(), $this->getSubmitted());
			}
			else if(strtolower($input->type) == "checkbox")
			{
				$element = new CheckBox($input, $this->getStyle(), $this->getMethod(), $this->getSubmitted());
			}
			else if(strtolower($input->type) == "radio")
			{
				$element = new Radio($input, $this->getStyle(), $this->getMethod(), $this->getSubmitted());
			}
			else if(strtolower($input->type) == "password")
			{
				$element = new Password($input, $this->getStyle(), $this->getMethod(), $this->getSubmitted());
			}
			else if(strtolower($input->type) == "select")
			{
				$element = new Select($input, $this->getStyle(), $this->getMethod(), $this->getSubmitted());
			}
			else if(strtolower($input->type) == "button")
			{
				$element = new Button($input, $this->getStyle(), $this->getMethod(), $this->getSubmitted());
			}
			else if(strtolower($input->type) == "inputbutton")
			{
				$element = new InputButton($input, $this->getStyle(), $this->getMethod(), $this->getSubmitted());
			}
			else if(strtolower($input->type) == "submit")
			{
				$element = new Submit($input, $this->getStyle(), $this->getMethod(), $this->getSubmitted());
			}
			else if(strtolower($input->type) == "reset")
			{
				$element = new Reset($input, $this->getStyle(), $this->getMethod(), $this->getSubmitted());
			}
			else if(strtolower($input->type) == "hidden")
			{
				$element = new Hidden($input, $this->getStyle(), $this->getMethod(), $this->getSubmitted());
			}
			else if(strtolower($input->type) == "file")
			{
				$element = new File($input, $this->getStyle(), $this->getMethod(), $this->getSubmitted());
				$this->setEncodeType("multipart/form-data"); // required for forms with file uploads
				$xml = $input->save;
			}
			else if(strtolower($input->type) == "textarea")
			{
				$element = new TextArea($input, $this->getStyle(), $this->getMethod(), $this->getSubmitted());
			}
			else if(strtolower($input->type) == "calendar")
			{
				$element = new Calendar($input, $this->getStyle(), $this->getMethod(), $this->getSubmitted());
			}
			else if(strtolower($input->type) == "label")
			{
				$element = new Label($input, $this->getStyle(), $this->getMethod(), $this->getSubmitted());
			}
			else if(strtolower($input->type) == "image")
			{
				$element = new Image($input, $this->getStyle(), $this->getMethod(), $this->getSubmitted());
			}
			else if(strtolower($input->type) == "dropdown")
			{
				$element = new DropDown($input, $this->getStyle(), $this->getMethod(), $this->getSubmitted());
			}
			else if(strtolower($input->type) == "unorderedlist")
			{
				$element = new UnorderedList($input, $this->getStyle(), $this->getMethod(), $this->getSubmitted());
			}
			else
			{
				echo "Unrecognized Type: " . ((string) $input->type);
			}
				

			if(isset($element))
			{
				$this->addElement($element, $xml);
			}
		}
	}

	public function updateElementValue($elementName, $elementValue, $isPartOfArray=false, $checked=false)
	{
		if($isPartOfArray && substr($elementName, -2) != '[]')
		{
			$elementName = $elementName . '[]';
		}
		foreach($this->getElements() as $element)
		{
			if($element->getName() == $elementName)
			{
				if($isPartOfArray)
				{
					if(($element instanceof CheckBox || $element instanceof Radio) && $elementValue = $element->getValue())
					{
						$element->setChecked($checked);
					}
				}
				else
				{
					$element->setValue($elementValue);
				}
			}
		}
	}
}
?>