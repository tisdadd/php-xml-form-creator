<?php
/*
 * Copyright Michael Adsit 2012.
 */
include_once('Input.php');

class CheckBox extends Input
{
	protected $defaultChecked;
	protected $checked;

	public function __construct($xml, $style, $method, $submitted)
	{
		parent::__construct($xml, $style, $method, $submitted);
		$this->generateUniqueElements($xml);
	}

	public function getHTML()
	{
		$checked = $this->getChecked();
		$style = $this->getStyle();
		$toReturn = "";
		$toReturn .= $this->generateErrorString();
			
		if($style == "table")
		{
			$toReturn .="<tr><td align=\"right\"><input type =\"checkbox\" " . trim($this->getGenericElementString());
		}
		else
		{
			$toReturn .= "<input type =\"checkbox\" " . trim($this->getGenericElementString());
		}

		if(isset($checked) && $checked)
		{
			$toReturn .= " checked=\"checked\"";
		}
			
		$toReturn .= " />";
			
		if($style == "table")
		{
			$toReturn .= "</td><td align=\"left\">";
		}
			
		$label = trim($this->getLabel());
		$labelClass = $this->getLabelClass();
		if($labelClass != "")
		{
			$label = '<span class="' . $labelClass .'">' . $label . '</span>';
		}
			
		$toReturn .= $label;

		if($style == "table")
		{
			$toReturn .= "</td></tr>";
		}
		else if ($style == "row")
		{
			$toReturn .= "<br />";
		}

		return $toReturn;
	}

	protected function generateUniqueElements()
	{
		$xml = $this->xml;
		if(isset($xml) && $xml instanceof SimpleXMLElement)
		{
			$this->setDefaultChecked($xml->defaultchecked);
		}
	}

	protected function setDefaultChecked($defaultChecked)
	{
		$this->defaultChecked = (boolean) ((string) $defaultChecked);
	}

	public function getDefaultChecked()
	{
		if(!isset($this->defaultChecked))
		{
			$this->setDefaultChecked(false);
		}
		return $this->defaultChecked;
	}

	public function setChecked($checked)
	{
		$this->checked = $checked;
	}

	public function getChecked()
	{
		if(!isset($this->checked))
		{
			$this->setChecked($this->getDefaultChecked());
		}
		return $this->checked;
	}

	public function getKeyValueArray()
	{
		$key = $this->getPHPVariable();
		if(!isset($key) || $key == "")
		{
			return false;
		}
		$value = $this->getChecked();

		if($this->getChecked())
		{
			$toReturn = array($key=>$value);
		}
		else
		{
			$toReturn = array();
		}
		return $toReturn;
	}

	public function resetErrors()
	{
		$xml = $this->xml;
		$method = $this->getMethod();
		$name = $this->getName();
		$submitted = $this->getSubmitted();

		/*
		 * For the arrays...
		 */
		if(substr($name, -2) == "[]")
		{
			$name = substr($name, 0, -2);
		}

		$toCheck = array();

		if(strtolower($method) == "post" && isset($_POST[$name]))
		{
			$toCheck = $_POST[$name];
		}
		else if(strtolower($method) == "get" && isset($_GET[$name]))
		{
			$toCheck = $_GET[$name];
		}
			
		if($submitted)
		{
				
			if(in_array($this->getValue(), $toCheck))
			{
				$this->setChecked(true);
			}

			foreach($xml->error as $error)
			{
				$this->addError(new CustomError($xml->error, $toCheck));
			}
		}
	}
}
?>