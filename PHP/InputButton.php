<?php
/*
 * Copyright Michael Adsit 2012.
 */

include_once('Input.php');

class InputButton extends Input
{
	public function __construct($xml, $style, $method, $submitted)
	{
		parent::__construct($xml, $style, $method, $submitted);
	}

	public function getHTML()
	{
		$style = $this->getStyle();
		$toReturn = "";
		$toReturn .= $this->generateErrorString();
			
		if($style == "table")
		{
			$toReturn .="<tr><td align=\"left\"><input type=\"button\" " . trim($this->getGenericElementString());
		}
		else
		{
			$toReturn .= trim("<input type=\"button\" " . trim($this->getGenericElementString()));
		}

		$toReturn .= " />";

		if($style == "table")
		{
			$toReturn .= "</td></tr>";
		}
		else if ($style == "row")
		{
			$toReturn .= "<br />";
		}

		return $toReturn;
	}

	protected function setLabel($label)
	{
		$this->setValue($label);
	}

	protected function getLabel()
	{
		return $this->getValue();
	}

	public function setValue($value)
	{
		$currentValue= $this->getValue();
		parent::setValue($value);
		if($this->getValue() == "")
		{
			parent::setValue($currentValue);
		}
	}
}

?>