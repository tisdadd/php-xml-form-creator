<?php
/*
 * Copyright Michael Adsit 2012.
 */

include_once('Input.php');
include_once('Option.php');
include_once('OptionGroup.php');

class Select extends Input
{
	private $elements; // elements can be either an optiongroup or an option...
	private $multiple; // if multiple values can be selected
	private $optionsVisible; // size in HTML

	public function __construct($xml, $style, $method, $submitted)
	{
		parent::__construct($xml, $style, $method, $submitted);
		$this->generateUniqueElements($xml);
	}

	public function getHTML()
	{
		$optionsVisible = $this->getOptionsVisible();
		$multiple = $this->getMultiple();
		$label = $this->getLabel();
		$labelClass = $this->getLabelClass();
		if($labelClass != "")
		{
			$label = '<span class="' . $labelClass .'">' . $label . '</span>';
		}
		$style = $this->getStyle();
		$toReturn = "";
		$toReturn .= $this->generateErrorString();
			
		if($style == "table")
		{
			$toReturn .="<tr><td align=\"right\">" . trim($label) . "</td><td align=\"left\">" . "<select " . trim($this->getGenericElementString());
		}
		else
		{
			$toReturn .= trim($label . " <select " . trim($this->getGenericElementString()));
		}
			
		if(isset($optionsVisible) && $optionsVisible > 0)
		{
			$toReturn .= " size=\"" . $optionsVisible . "\"";
		}
			
		if(isset($multiple) && $multiple)
		{
			$toReturn .= " multiple=\"multiple\"";
		}
			
		$toReturn .= ">";
			
		$elements = $this->getElements();
		$value = $this->getValue();

		foreach($elements as $element)
		{
			$toReturn .= $element->getHTML($value);
		}
			
		$toReturn .= "</select>";
			
		if($style == "table")
		{
			$toReturn .= "</td></tr>";
		}
		else if ($style == "row")
		{
			$toReturn .= "<br />";
		}
			
		return $toReturn;
	}

	private function generateUniqueElements($xml)
	{
		$this->setElements($xml);
		$this->setMultiple($xml->multiple);
		$this->setOptionsVisible($xml->optionsvisible);
	}

	private function setMultiple($multiple)
	{
		if(!isset($multiple))
		{
			$multiple = false;
		}
			
		$multiple = (bool)((string) $multiple);
			
		$this->multiple = $multiple;
	}

	private function getMultiple()
	{
		if(!isset($this->multiple))
		{
			$this->setMultiple(false);
		}
		return $this->multiple;
	}

	private function setElements($elements)
	{
		$this->elements = array();
		if(!isset($elements))
		{
			$elements = array();
		}
			
		if($elements instanceof SimpleXMLElement)
		{
			foreach($elements->children() AS $element)
			{
				if($element->getName() == "option")
				{
					$this->addElement(new Option($element));
				}
				else if($element->getName() == "optiongroup")
				{
					$this->addElement(new OptionGroup($element));
				}
			}
		}
		else if(is_array($elements))
		{
			foreach($elements AS $element)
			{
				$this->addElement($element);
			}
		}
		else
		{
			$this->elements = array();
		}
	}

	private function addElement($element)
	{
		if(!isset($this->elements) || !is_array($this->elements))
		{
			$this->elements = array();
		}
			
		if($element instanceof Option || $element instanceof OptionGroup)
		{
			$this->elements[] = $element;
		}
	}

	private function getElements()
	{
		if(!isset($this->elements))
		{
			$this->setElements(array());
		}
		return $this->elements;
	}

	private function setOptionsVisible($optionsVisible)
	{
		if(!isset($optionsVisible))
		{
			$optionsVisible = -1;
		}
		$optionsVisible = (string) $optionsVisible;
			
		if(is_int($optionsVisible))
		{
			$this->optionsVisible = $optionsVisible;
		}
		else
		{
			$this->optionsVisible = -1;
		}
	}

	private function getOptionsVisible()
	{
		if(!isset($this->optionsVisible))
		{
			$this->setOptionsVisible(-1);
		}
			
		return $this->optionsVisible;
	}

	public function setName($name)
	{
		parent::setName($name);
		$multiple = $this->getMultiple();
		$name = $this->getName();
		if(isset($multiple) && $multiple && substr($name, -2) != "[]")
		{
			$name = ($name . "[]");
		}
			
		$this->name = $name;
	}

	public function setValue($value)
	{
		if(is_array($value))
		{
			$this->value = $value;
		}
		else
		{
			$this->value = (string) $value;
		}
	}
}
?>