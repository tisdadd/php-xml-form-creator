<?php
/*
 * Copyright Michael Adsit 2012.
 */
include_once('Input.php');

class TextBox extends Input
{
	protected $cols;
	protected $maxLength;

	public function __construct($xml, $style, $method, $submitted)
	{
		parent::__construct($xml, $style, $method, $submitted);
		$this->generateUniqueElements($xml);
	}

	protected function generateUniqueElements()
	{
		$xml = $this->xml;
		if(isset($xml) && $xml instanceof SimpleXMLElement)
		{
			$this->setCols($xml->cols);
			$this->setMaxLength($xml->maxlength);
		}
	}

	public function getHTML()
	{
		$cols = $this->getCols();
		$maxLength = $this->getMaxLength();
		$label = $this->getLabel();
		$labelClass = $this->getLabelClass();
		if($labelClass != "")
		{
			$label = '<span class="' . $labelClass .'">' . $label . '</span>';
		}
		$style = $this->getStyle();
		$toReturn = "";
		$toReturn .= $this->generateErrorString();
			
		if($style == "table")
		{
			$toReturn .="<tr><td align=\"right\">" . trim($label) . "</td><td align=\"left\">" . "<input type=\"text\" " . trim($this->getGenericElementString());
		}
		else
		{
			$toReturn .= trim($label . " <input type=\"text\" " . trim($this->getGenericElementString()));
		}
			
		if(isset($cols) && is_numeric($cols))
		{
			$toReturn .= " size=\"" . $cols . "\"";
		}
			
		if(isset($maxLength) && is_numeric($maxLength))
		{
			$toReturn .= " maxlength=\"" . $maxLength . "\"";
		}
			
		$toReturn .= " />";
			
		if($style == "table")
		{
			$toReturn .= "</td></tr>";
		}
		else if ($style == "row")
		{
			$toReturn .= "<br />";
		}
			
		return $toReturn;
	}

	protected function setCols($cols)
	{
		$this->cols = (string) $cols;
	}

	protected function getCols()
	{
		return $this->cols;
	}

	protected function setMaxLength($maxLength)
	{
		$this->maxLength = (string) $maxLength;
	}

	protected function getMaxLength()
	{
		return $this->maxLength;
	}
}
?>