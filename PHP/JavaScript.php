<?php

/*
 * Copyright Michael Adsit 2012.
 */

class JavaScript
{
	private $onBlur;
	private $onChange;
	private $onClick;
	private $onDblClick;
	private $onFocus;
	private $onMouseDown;
	private $onMouseMove;
	private $onMouseOut;
	private $onMouseOver;
	private $onMouseUp;
	private $onKeyDown;
	private $onKeyPress;
	private $onKeyUp;
	private $onSelect;

	public function __construct($xml)
	{
		$this->setOnBlur($xml->onblur);
		$this->setOnChange($xml->onchange);
		$this->setOnClick($xml->onclick);
		$this->setOnDblClick($xml->ondblclick);
		$this->setOnFocus($xml->onfocus);
		$this->setOnMouseDown($xml->onmousedown);
		$this->setOnMouseMove($xml->onmousemove);
		$this->setOnMouseOut($xml->onmouseout);
		$this->setOnMouseOver($xml->onmouseover);
		$this->setOnMouseUp($xml->onmouseup);
		$this->setOnKeyDown($xml->onkeydown);
		$this->setOnKeyPress($xml->onkeypress);
		$this->setOnKeyUp($xml->onkeyup);
		$this->setOnSelect($xml->onselect);
	}

	public function generateHTMLString()
	{
		$onBlur = $this->getOnBlur();
		$onChange = $this->getOnChange();
		$onClick = $this->getOnClick();
		$onDblClick = $this->getOnDblClick();
		$onFocus = $this->getOnFocus();
		$onKeyDown = $this->getOnKeyDown();
		$onKeyPress = $this->getOnKeyPress();
		$onKeyUp = $this->getOnKeyUp();
		$onMouseDown = $this->getOnMouseDown();
		$onMouseMove = $this->getOnMouseMove();
		$onMouseOut = $this->getOnMouseOut();
		$onMouseOver = $this->getOnMouseOver();
		$onMouseUp = $this->getOnMouseUp();
		$onSelect = $this->getOnSelect();
			
		$toReturn = "";
			
		if(isset($onBlur) && $onBlur <> "")
		{
			$toReturn .= " onblur=\"" . $onBlur . "\"";
		}
			
		if(isset($onChange) && $onChange <> "")
		{
			$toReturn .= " onchange=\"" . $onChange . "\"";
		}
			
		if(isset($onClick) && $onClick <> "")
		{
			$toReturn .= " onclick=\"" . $onClick . "\"";
		}
			
		if(isset($onDblClick) && $onDblClick <> "")
		{
			$toReturn .= " ondblclick=\"" . $onDblClick . "\"";
		}
			
		if(isset($onFocus) && $onFocus <> "")
		{
			$toReturn .= " onfocus=\"" . $onFocus . "\"";
		}
			
		if(isset($onKeyDown) && $onKeyDown <> "")
		{
			$toReturn .= " onkeydown=\"" . $onKeyDown . "\"";
		}
			
		if(isset($onKeyPress) && $onKeyPress <> "")
		{
			$toReturn .= " onkeypress=\"" . $onKeyPress . "\"";
		}
			
		if(isset($onKeyUp) && $onKeyUp <> "")
		{
			$toReturn .= " onkeyup=\"" . $onKeyUp . "\"";
		}
			
		if(isset($onMouseDown) && $onMouseDown <> "")
		{
			$toReturn .= " onmousedown=\"" . $onMouseDown . "\"";
		}
			
		if(isset($onMouseMove) && $onMouseMove <> "")
		{
			$toReturn .= " onmousemove=\"" . $onMouseMove . "\"";
		}
			
		if(isset($onMouseOut) && $onMouseOut <> "")
		{
			$toReturn .= " onmouseout=\"" . $onMouseOut . "\"";
		}
			
		if(isset($onMouseOver) && $onMouseOver <> "")
		{
			$toReturn .= " onmouseover=\"" . $onMouseOver . "\"";
		}
			
		if(isset($onMouseUp) && $onMouseUp <> "")
		{
			$toReturn .= " onmouseup=\"" . $onMouseUp . "\"";
		}
			
		if(isset($onSelect) && $onSelect <> "")
		{
			$toReturn .= " onselect=\"" . $onSelect . "\"";
		}
			
		return $toReturn;
	}

	private function setOnBlur($onBlur)
	{
		$this->onBlur=(string) $onBlur;
	}

	private function getOnBlur()
	{
		if(!isset($this->onBlur))
		{
			$this->setOnBlur("");
		}
			
		return $this->onBlur;
	}

	private function setOnChange($onChange)
	{
		$this->onChange = (string) $onChange;
	}

	private function getOnChange()
	{
		if(!isset($this->onChange))
		{
			$this->setOnChange("");
		}
			
		return $this->onChange;
	}

	private function setOnClick($onClick)
	{
		$this->onClick = (string) $onClick;
	}

	private function getOnClick()
	{
		if(!isset($this->onClick))
		{
			$this->setOnClick("");
		}

		return $this->onClick;
	}

	private function setOnDblClick($onDblClick)
	{
		$this->onDblClick = (string) $onDblClick;
	}

	private function getOnDblClick()
	{
		if(!isset($this->OnDblClick))
		{
			$this->setOnDblClick("");
		}

		return $this->onDblClick;
	}

	private function setOnFocus($onFocus)
	{
		$this->onFocus = (string) $onFocus;
	}

	private function getOnFocus()
	{
		if(!isset($this->onFocus))
		{
			$this->setOnFocus("");
		}

		return $this->onFocus;
	}

	private function setOnKeyDown($onKeyDown)
	{
		$this->onKeyDown = (string) $onKeyDown;
	}

	private function getOnKeyDown()
	{
		if(!isset($this->onKeyDown))
		{
			$this->setOnKeyDown("");
		}

		return $this->onKeyDown;
	}

	private function setOnKeyPress($onKeyPress)
	{
		$this->onKeyPress = (string) $onKeyPress;
	}

	private function getOnKeyPress()
	{
		if(!isset($this->onKeyPress))
		{
			$this->setOnKeyPress("");
		}

		return $this->onKeyPress;
	}

	private function setOnKeyUp($onKeyUp)
	{
		$this->onKeyUp = (string) $onKeyUp;
	}

	private function getOnKeyUp()
	{
		if(!isset($this->onKeyUp))
		{
			$this->setonKeyUp("");
		}

		return $this->onKeyUp;
	}

	private function setOnMouseDown($onMouseDown)
	{
		$this->onMouseDown = (string) $onMouseDown;
	}

	private function getOnMouseDown()
	{
		if(!isset($this->onMouseDown))
		{
			$this->setOnMouseDown("");
		}

		return $this->onMouseDown;
	}

	private function setOnMouseMove($onMouseMove)
	{
		$this->onMouseMove = (string) $onMouseMove;
	}

	private function getOnMouseMove()
	{
		if(!isset($this->onMouseMove))
		{
			$this->setOnMouseMove("");
		}

		return $this->onMouseMove;
	}

	private function setOnMouseOut($onMouseOut)
	{
		$this->onMouseOut = (string) $onMouseOut;
	}

	private function getOnMouseOut()
	{
		if(!isset($this->onMouseOut))
		{
			$this->setOnMouseOut("");
		}

		return $this->onMouseOut;
	}

	private function setOnMouseOver($onMouseOver)
	{
		$this->onMouseOver = (string) $onMouseOver;
	}

	private function getOnMouseOver()
	{
		if(!isset($this->onMouseOver))
		{
			$this->setOnMouseOver("");
		}

		return $this->onMouseOver;
	}

	private function setOnMouseUp($onMouseUp)
	{
		$this->onMouseUp = (string) $onMouseUp;
	}

	private function getOnMouseUp()
	{
		if(!isset($this->onMouseUp))
		{
			$this->setOnMouseUp("");
		}

		return $this->onMouseUp;
	}

	private function setOnSelect($onSelect)
	{
		$this->onSelect = (string) $onSelect;
	}

	private function getOnSelect()
	{
		if(!isset($this->onSelect))
		{
			$this->setOnSelect("");
		}

		return $this->onSelect;
	}
}
?>