<?php

/*
 * Copyright Michael Adsit 2012.
 */

class Option
{
	private $value;
	private $label;
	public static $numOptions = 0;

	public function __construct($xml)
	{
		Option::$numOptions++;
		$this->generateUniqueElements($xml);
	}

	public function getHTML($selectedValue)
	{
		$value = $this->getValue();
		$label = $this->getLabel();
		$toReturn = "<option value=\"" . $value . "\"";
		if(isset($selectedValue) && (($selectedValue == $value) || (is_array($selectedValue) && in_array($value, $selectedValue))))
		{
			$toReturn .= " selected=\"selected\"";
		}
			
		$toReturn .= ">" . $label . "</option>";
			
		return $toReturn;
	}

	private function generateUniqueElements($xml)
	{
		if($xml instanceof SimpleXMLElement)
		{
			$this->setValue($xml->value);
			$this->setLabel($xml->label);
		}
		else if(is_array($xml))
		{
			if(isset($xml[0]))
			{
				setValue($xml[0]);
			}

			if(isset($xml[1]))
			{
				setLabel($xml[1]);
			}
		}
	}

	private function setValue($value)
	{
		if(!isset($value) || (string) $value =="")
		{
			$value = "Value" . Option::$numOptions;
		}
			
		$this->value = (string) $value;
	}

	private function getValue()
	{
		if(!isset($this->value))
		{
			$this->setValue("");
		}
			
		return $this->value;
	}

	private function setLabel($label)
	{
		if(!isset($label) || (string) $label =="")
		{
			$label = "Option" . Option::$numOptions;
		}

		$this->label = (string) $label;
	}

	private function getLabel()
	{
		if(!isset($this->label))
		{
			$this->setLabel("");
		}
			
		return $this->label;
	}
}
?>