<?php
/*
 * Copyright Michael Adsit 2012.
 */
include_once('Input.php');

class Button extends Input
{
	private $type;

	public function __construct($xml, $style, $method, $submitted)
	{
		parent::__construct($xml, $style, $method, $submitted);
		$this->generateUniqueElements($xml);
	}

	public function getHTML()
	{
		$type = $this->getType();
		$label = $this->getLabel();
		$labelClass = $this->getLabelClass();

		if($labelClass != "")
		{
			$label = '<span class="' . $labelClass .'">' . $label . '</span>';
		}
		$style = $this->getStyle();
		$toReturn = "";
		$toReturn .= $this->generateErrorString();

		if($style == "table")
		{
			$toReturn .="<tr><td align=\"left\"><button " . trim($this->getGenericElementString());
		}
		else
		{
			$toReturn .= trim("<button " . trim($this->getGenericElementString()));
		}

		if(isset($type) && $type != "")
		{
			$toReturn .= " type=\"" . $type . "\"";
		}

		$toReturn .= ">" . $label . "</button>";
		if($style == "table")
		{
			$toReturn .= "</td></tr>";
		}
		else if ($style == "row")
		{
			$toReturn .= "<br />";
		}

		return $toReturn;
	}

	private function generateUniqueElements($xml)
	{
		$this->setType($xml->buttontype);
	}


	private function validTypes()
	{
		return array("", "button", "reset", "submit");
	}

	private function setType($type)
	{
		$validTypes = $this->validTypes();
		if(!isset($type))
		{
			$type = "button";
		}
			
		$type = (string) $type;
			
		if(in_array($type, $validTypes))
		{
			$this->type = $type;
		}
		else if(isset($validTypes[0]))
		{
			$this->type = $validTypes[0];
		}
		else
		{
			$this->type = "";
		}
	}

	private function getType()
	{
		if(!isset($this->type))
		{
			$this->setType("button");
		}
			
		return $this->type;
	}
}
?>