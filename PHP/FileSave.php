<?php
/*
 * Copyright Michael Adsit 2012.
 */
/*
 * This is a default FileSave extension type.
 * It will allow multiple uploads, and has a small amount of security.
 */

class FileSave
{
	private $allowedTypes;
	private $validExtensions;
	private $location;
	private $allowedSize;
	private $errors;
	private $fileInfo;
	private $fileSize;
	private $fileSaved;

	public function __construct($xml, $allowedTypes, $fileInfo, $validExtensions = array(), $location = "", $allowedSize = -1)
	{
		if (isset($xml) && $xml instanceof SimpleXMLElement) {
			if ($allowedSize == -1) {
				$this->setAllowedSize($xml->allowedSize);
			} else {
				$this->setAllowedSize($allowedSize);
			}

			if (isset($xml->allowed) && isset($xml->allowed->type[0]) && (string) $xml->allowed->type[0] != "") {
				$this->setAllowedTypes($xml->allowed);
			} else {
				$this->setAllowedTypes($allowedTypes);
			}

			$this->setValidExtensions($xml);
			if (is_array($validExtensions)) {
				foreach ($validExtensions as $valid) {
					$this->addValidExtension($valid);
				}
			}

			if (isset($xml->location) && (string) $xml->location != "") {
				$this->setLocation($xml->location);
			} else {
				$this->setLocation($location);
			}
		} else {
			$this->setAllowedSize($allowedSize);
			$this->setAllowedTypes($allowedTypes);
			$this->setLocation($location);
			$this->setType($type);
		}
		$this->setFileInfo($fileInfo);
		$this->setErrors($xml);
		$this->saveFile();
	}

	private function addAllowedType($type)
	{
		if (!isset($this->allowedTypes) || !(is_array($this->allowedTypes))) {
			$this->allowedTypes = array();
		}
		if (!isset($type)) {
			$type = "";
		}

		if ($type != "") {
			$this->allowedTypes[] = strtolower((string) $type);
		}
	}

	private function setAllowedTypes($allowedTypes)
	{
		if (!isset($allowedTypes)) {
			$allowedTypes = array();
		}
		if (is_array($allowedTypes)) {
			$this->allowedTypes = $allowedTypes;
		} else if ($allowedTypes instanceof SimpleXMLElement) {
			foreach ($allowedTypes->type as $type) {
				$this->addAllowedType($type);
			}
		} else {
			$this->allowedTypes = array();
		}
	}

	private function getAllowedTypes()
	{
		if (!isset($this->allowedTypes)) {
			$this->setAllowedTypes(array());
		}
		return $this->allowedTypes;
	}

	private function addValidExtension($extension)
	{
		if (!isset($this->validExtensions) || !(is_array($this->validExtensions))) {
			$this->validExtensions = array();
		}
		if (!isset($extension)) {
			$extension = "";
		}

		if ($extension != "") {
			$this->validExtensions[] = (string) $extension;
		}
	}

	private function setValidExtensions($validExtensions)
	{
		if (!isset($validExtensions)) {
			$validExtensions = array();
		}
		if (is_array($validExtensions)) {
			$this->validExtensions = $validExtensions;
		} else if ($validExtensions instanceof SimpleXMLElement && isset($validExtensions->extension)) {
			foreach ($validExtensions->extension as $extension) {
				$this->addValidExtension($extension);
			}
		} else {
			$this->validExtensions = array();
		}
	}

	private function getValidExtensions()
	{
		if (!isset($this->validExtensions)) {
			$this->setValidExtensions(array());
		}
		return $this->validExtensions;
	}

	private function setLocation($location)
	{
		if (!isset($location) || $location == "") {
			$location = "/Uploads/UserUploads/IP" . $_SERVER['REMOTE_ADDR'];
		}

		$this->location = (string) $location;
	}

	private function getLocation()
	{
		if (!isset($this->location) || $this->location == "") {
			$this->setLocation();
		}

		return $this->location . "/";
	}

	private function setFileInfo($fileInfo)
	{
		if (isset($fileInfo) && is_array($fileInfo)) {
			$this->fileInfo = $fileInfo;
		} else {
			$this->fileInfo = false;
		}
	}

	public function getFileInfo()
	{
		if (isset($this->fileInfo) && is_array($this->fileInfo)) {
			return $this->fileInfo;
		}
		return false;
	}

	/*
	 * Removes all .s but the final file extenstion one to avoid multi-file extension attacks, and returns the extensions
	 */
	private function getExtensions()
	{
		$fileInfo = $this->getFileInfo();
		$toReturn = array();
		if (is_array($fileInfo)) {
			if (is_array($fileInfo["name"]) && isset($fileInfo["name"][0])) {
				for ($j = 0; $j < count($fileInfo["name"]); $j++) {
					$name = $fileInfo["name"][$j];
					$seperated = explode(".", $name);
					if (count($seperated) > 2) {
						$name = "";
						for ($i = 0; $i < count($seperated) - 1; $i++) {
							$name .= $seperated[$i];
						}
						$name .= "." . end($seperated);
					}
					$fileInfo["name"][$j] = $name;
					$seperated = explode(".", $name);
					if (count($seperated) > 2 || count($seperated) == 1) {
						$toReturn[] = "Unknown";
					}
					$toReturn[] = end($seperated);
				}
			} else {
				$name = $fileInfo["name"];
				$seperated = explode(".", $name);
				if (count($seperated) > 2) {
					$name = "";
					for ($i = 0; $i < count($seperated) - 1; $i++) {
						$name .= $seperated[$i];
					}
					$name .= "." . end($seperated);
				}
				$fileInfo["name"] = $name;
				$seperated = explode(".", $name);
				if (count($seperated) > 2 || count($seperated) == 1) {
					$toReturn[] = "Unknown";
				}
				$toReturn[] = end($seperated);
			}
		} else {
			$toReturn[] = "Unknown";
		}
		return $toReturn;
	}

	private function getMimeTypes()
	{
		if (function_exists("finfo_open")) {
			$finfo = finfo_open();
			$mimeTypes = array();
			$fileInfo = $this->getFileInfo();
			if (isset($fileInfo["tmp_name"])) {
				if (is_array($fileInfo["tmp_name"])) {
					foreach ($fileInfo["tmp_name"] as $file) {
						$mimeTypes[] = finfo_file($finfo, $file, FILEINFO_MIME);
					}
				} else {
					$file = $fileInfo["tmp_name"];
					$mimeTypes[] = finfo_file($finfo, $file, FILEINFO_MIME);
				}
			}
			finfo_close($finfo);
		} else { // less secure... not recommended, but need something...
			$mimeTypes = array();
			$fileInfo = $this->getFileInfo();
			if (isset($fileInfo["type"])) {
				if (is_array($fileInfo["type"])) {
					foreach ($fileInfo["type"] as $type) {
						$mimeTypes[] = $type;
					}
				} else {
					$mimeTypes[] = $fileInfo["type"];
				}
			}
		}
		return $mimeTypes;
	}

	private function setAllowedSize($allowedSize)
	{
		$allowedSize = (string) $allowedSize;
		if (!isset($allowedSize) || $allowedSize == "") {
			$allowedSize = -1;
		}

		$this->allowedSize = (int) $allowedSize;
	}

	private function getAllowedSize()
	{
		if (!isset($this->allowedSize)) {
			$this->setAllowedSize(-1);
		}

		return $this->allowedSize;
	}

	private function setErrors($errors)
	{
		$this->errors = array();
		$allowedSize = $this->getAllowedSize();

		$fileInfo = $this->getFileInfo();

		if (!isset($errors)) {
			$errors = array();
		}

		if ($fileInfo === false) {
			$this->errors[] = "File information not located.";
		} else {
			$fileErrors = array();
			if (is_array($fileInfo["size"]) && isset($fileInfo["size"][0])) {
				$fileSize = 0;
				foreach ($fileInfo["size"] as $size) {
					if ($fileSize < $size) {
						$fileSize = $size;
					}
				}

				foreach ($fileInfo["error"] as $fileError) {
					if ($fileError <> 0) {
						$fileErrors[] = "Generic file error #" . $fileError . ".";
					}
				}
			} else {
				$fileSize = $fileInfo["size"];
				if ($fileInfo["error"] <> 0) {
					$fileErrors[] = "Generic file error #" . $fileInfo["error"] . ".";
				}
			}
		}

		if (!$this->checkValidType()) {
			$this->errors[] = "Type of file is not allowed, please use only the following: " . implode(", ", $this->getAllowedTypes()) . ".";
		}

		if (!$this->checkValidExtension()) {
			$this->errors[] = "File extension is not allowed, please use only the following: " . implode(", ", $this->getValidExtensions()) . ".";
		}

		if ((!($allowedSize == -1)) && isset($fileSize) && $allowedSize < $fileSize) {
			$this->errors[] = "File is too large, please submit a file under " . $fileSize . " bytes.";
		}

		if ($errors instanceof SimpleXMLElement && isset($errors->error)) {
			$temp = array();
			foreach ($errors->error as $error) {
				$temp[] = new CustomError($errors);
			}
			$errors = $temp;
		}

		if (isset($fileErrors) && is_array($fileErrors)) {
			$this->errors = array_merge($this->errors, $fileErrors);
		}

		if (is_array($errors)) {
			$this->errors = array_merge($this->errors, $errors);
		}
	}

	private function getErrors()
	{
		if (!isset($errors)) {
			$this->setErrors(array());
		}
		return $this->errors;
	}

	public function getErrorString()
	{
		$toReturn = "";
		$errors = $this->getErrors();
		foreach ($errors as $error) {
			if ($error instanceof Error) {
				$toReturn .= $error->getErrorMessage();
			} else {
				$toReturn .= $error . "<br />";
			}
		}
		return $toReturn;
	}

	private function checkValidExtension()
	{
		$validExtensions = $this->getValidExtensions();
		$extensions = $this->getExtensions();
		if (!isset($validExtensions)) { // don't care about extension type
			return true;
		}
		if (!is_array($validExtensions)) {
			return false;
		}
		if (count($validExtensions) == 0) { // don't care about extension type
			return true;
		}

		foreach ($extensions as $toCheck) {
			if (!in_array(strtolower($toCheck), $validExtensions)) {
				return false;
			}
		}
		return true;
	}

	private function checkValidType()
	{

		$validTypes = $this->getAllowedTypes();
		$type = $this->getMimeTypes();
		if (!isset($validTypes)) { // don't really care what type it is... very low security
			return true;
		}
		if (!is_array($validTypes)) {
			return false;
		}
		if (count($validTypes) == 0) { // don't really care what type it is, as wouldn't have an upload avaible with no valid types
			return true;
		}

		if (!is_array($type)) {
			$temp = $type;
			$type = array();
			$type[] = $temp;
		}
		$toReturn = false;
		foreach ($validTypes as $validType) {

			$toReturn = false;
			foreach ($type as $currentType) {
				echo ($currentType . $validType);
				if (strtolower($currentType) == strtolower($validType)) {
					$toReturn = true;
				} else if (substr($validType, -2) == "/*" && !(strripos($currentType, "/") === false) && (strtolower(substr($currentType, 0, strripos($currentType, "/"))) == strtolower(substr($validType, 0, strripos($validType, "/"))))) {
					$toReturn = true;
				}
			}
		}

		return $toReturn;
	}

	private function setFileSaved($fileSaved)
	{
		if (isset($fileSaved) && is_bool($fileSaved)) {
			$this->fileSaved = $fileSaved;
		} else {
			$this->fileSaved = false;
		}
	}

	public function getFileSaved()
	{
		if (isset($this->fileSaved) && is_bool($this->fileSaved)) {
			return $this->fileSaved;
		}
	}

	private function saveFile()
	{
		if (trim($this->getErrorString()) != "") {
			return false;
		}

		$location = $this->getLocation();
		$fileInfo = $this->getFileInfo();

		if (is_array($fileInfo["name"])) {
			$toReturn = true;
			for ($i = 0; $i < count($fileInfo["name"]); $i++) {
				$name = $fileInfo["name"][$i];
				$j = 0;
				if (!(file_exists($location) && is_dir($location))) {
					if (!mkdir($location, 0777, true)) {
						return false;
					}
				}
				while (file_exists($location . "/" . $name)) {
					$name = substr($fileInfo["name"][$i], 0, strripos($fileInfo["name"][$i], ".")) . "_" . $j . substr($fileInfo["name"][$i], strripos($fileInfo["name"][$i], "."));
					$j++;
				}
				$fileInfo["name"][$i] = $name;

				if (!move_uploaded_file($fileInfo["tmp_name"][$i], $location . $name)) {
					$toReturn = false;
				}
			}
			$this->fileInfo = $fileInfo;
			return $toReturn;
		} else {
			$name = $fileInfo["name"];
			$i = 0;
			if (!(file_exists($location) && is_dir($location))) {
				if (!mkdir($location, 0666, true)) {
					return false;
				}
			}
			while (file_exists($location . "/" . $name)) {
				$name = substr($fileInfo["name"], 0, strripos($fileInfo["name"], ".")) . "_" . $i . substr($fileInfo["name"], strripos($fileInfo["name"], "."));
				$i++;
			}

			$fileInfo["name"] = $name;
			$this->fileInfo = array($fileInfo);

			return move_uploaded_file($fileInfo["tmp_name"], $location . $name);
		}
	}
}
?>