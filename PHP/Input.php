<?php
/*
 * Copyright Michael Adsit 2012.
 */

include_once('CustomError.php');
include_once('JavaScript.php');
include_once('CheckBox.php');

abstract class Input
{
	protected $label;
	protected $name;
	protected $id;
	protected $value;
	protected $tabIndex;
	protected $class;
	protected $xml;
	protected $style;
	protected $errors;
	protected $method;
	protected $submitted;
	protected $disabled;
	protected $javaScript;
	protected $phpVariable;
	protected $defaultValue;
	protected $labelClass;
	protected $surroundingDivClass;
	protected $surroundingSpanClass;
	protected $cssStyle;

	abstract public function getHTML();

	public function __construct($xml, $style, $method, $submitted)
	{
		$this->xml = $xml;
		$this->setStyle($style);
		$this->setMethod($method);
		$this->setSubmitted($submitted);
		$this->generateElements();
	}

	protected function generateElements()
	{
		$xml = $this->xml;
		if(isset($xml) && $xml instanceof SimpleXMLElement)
		{
			$this->setLabel($xml->label);
			$this->setName($xml->name);
			$this->setID($xml->id);
			$this->setValue($xml->value);
			$this->setDefaultValue($xml->value);
			$this->setTabIndex($xml->tabindex);
			$this->setClass($xml->class);
			if(isset($xml->style) && trim((string) $xml->style) <> "")
			{
				$this->setStyle($xml->style);
			}

			$this->setJavaScript($xml->javascript);
			$this->setPHPVariable($xml->php);
			$this->setDisabled(($xml->disabled));
			$this->setLabelClass($xml->labelclass);
			$this->setSurroundingDivClass($xml->divclass);
			$this->setSurroundingSpanClass($xml->spanclass);
			$this->setCSSStyle($xml->cssstyle);

			if($this->getName() == "" && $this->getPHPVariable() != "")
			{
				$this->setName($this->getPHPVariable());
			}
			else if($this->getName() != "" && $this->getPHPVariable() == "")
			{
				$this->setPHPVariable($this->getName());
			}
		}
	}

	public function generateErrorString()
	{
		$toReturn = "";
		$errors = $this->getErrors();
		foreach($errors as $error)
		{
			if($error instanceof CustomError)
			{
				$message = $error->getErrorMessage();
				if(isset($message) && $message != "")
				{
					if($this->getStyle() == "table")
					{
						$toReturn .= "<tr><td colspan=\"2\"><span class=\"error\">" . $error->getErrorMessage() . "</span></td></tr>";
					}
					else
					{
						$toReturn .= "<span class=\"error\">" . $error->getErrorMessage() . "</span><br />";
					}
				}
			}
		}
		return $toReturn;
	}

	protected function getGenericElementString()
	{
		$name = $this->getName();
		$id = $this->getID();
		$value = "";
		if(!$this instanceof File)
		{
			$value = htmlentities($this->getValue());
		}
		
		$javaScript = $this->getJavaScript();
		$tabIndex = $this->getTabIndex();
		$class = $this->getClass();
		$disabled = $this->getDisabled();
		$cssStyle = $this->getCSSStyle();
			
		$toReturn = "";
			
		if(isset($class) && $class != "")
		{
			$toReturn .= " class=\"" . $class . "\"";
		}
			
		if(isset($name) && $name != "")
		{
			$toReturn .= " name=\"" . $name . "\"";
		}
			
		if(isset($id) && $id != "")
		{
			$toReturn .= " id=\"" . $id . "\"";
		}

		if(isset($cssStyle) && $cssStyle != "")
		{
			$toReturn .= " style=\"" . $cssStyle . "\"";
		}
			
		if(isset($value) && $value != "" && !$this instanceof Select && !$this instanceof TextArea && !$this instanceof File)
		{
			$toReturn .= " value=\"" . $value . "\"";
		}
			
		if(isset($onClick) && $onClick != "")
		{
			$toReturn .= " onclick=\"" . $onClick . "\"";
		}
			
		if(isset($tabIndex) && $tabIndex != "")
		{
			$toReturn .= " tabIndex=\"" . $tabIndex . "\"";
		}
			
		if(isset($disabled) && $disabled)
		{
			$toReturn .= " disabled=\"disabled\"";
		}
			
		if(isset($javaScript))
		{
			$toReturn .= $javaScript->generateHTMLString();
		}
			
		return $toReturn;
	}

	protected function setLabel($label)
	{
		$this->label = (string) $label;
	}

	protected function getLabel()
	{
		return $this->label;
	}

	public function setName($name)
	{
		$name = (string) $name;
		$name = str_replace(" ", "_", $name);
		$this->name = (string) $name;
	}

	public function getName()
	{
		return $this->name;
	}

	protected function setID($id)
	{
		$this->id = (string) $id;
	}

	protected function getID()
	{
		return $this->id;
	}

	public function setValue($value)
	{
		$this->value = (string) $value;
	}

	public function setDefaultValue($defaultValue)
	{
		$this->defaultValue = (string) $defaultValue;
	}

	public function getDefaultValue()
	{
		return $this->defaultValue;
	}

	public function getValue()
	{
		return $this->value;
	}

	protected function setJavaScript($javaScript)
	{
		if($javaScript instanceof JavaScript)
		{
			$this->javaScript = $javaScript;
		}
		else if($javaScript instanceof SimpleXMLElement)
		{
			$this->javaScript = new JavaScript($javaScript);
		}
		else
		{
			$this->javaScript = new JavaScript(new SimpleXMLElement("<javascript></javascript>"));
		}
	}

	protected function getJavaScript()
	{
		return $this->javaScript;
	}

	protected function setTabIndex($tabIndex)
	{
		$this->tabIndex = (string) $tabIndex;
	}

	protected function getTabIndex()
	{
		return $this->tabIndex;
	}

	protected function setClass($class)
	{
		$this->class = (string) $class;
	}

	protected function getClass()
	{
		return $this->class;
	}

	protected function setStyle($style)
	{
		$style = strtolower((string) $style);
		$validStyles = $this->validStyles();
		if(in_array($style, $validStyles))
		{
			$this->style = $style;
		}
		else if(isset($validStyles[0]))
		{
			$this->style = $validStyles[0];
		}
		else
		{
			$this->style = "none";
		}
	}

	public function getStyle()
	{
		if(!isset($this->style))
		{
			setStyle("");
		}
		return $this->style;
	}

	protected function validStyles()
	{
		return array("table", "row", "none");
	}

	protected function addError($error)
	{
		if(!isset($this->errors))
		{
			$this->errors = array();
		}
		$this->errors[] = $error;
	}

	public function getErrors()
	{
		if(!isset($this->errors))
		{
			$this->errors = array();
		}
		return $this->errors;
	}

	protected function setMethod($method)
	{
		if(in_array(strtolower($method), array("post", "get")))
		{
			$this->method = $method;
		}
		else
		{
			$this->method = "post";
		}
	}

	protected function getMethod()
	{
		if(!isset($this->method) || $this->method == "")
		{
			$this->setMethod("post");
		}
		return $this->method;
	}

	public function resetErrors()
	{
		$xml = $this->xml;
		$method = $this->getMethod();
		$name = $this->getName();
		$submitted = $this->getSubmitted();
			
		/*
		 * For the arrays...
		 */
		if(substr($name, -2) == "[]")
		{
			$name = substr($name, 0, -2);
		}
			
		if($this instanceof CheckBox || $this instanceof DropDown || $this instanceof Select)
		{
			$toCheck = array();
		}
		else
		{
			$toCheck = "";
		}

		if(strtolower($method) == "post" && isset($_POST[$name]))
		{
			$toCheck = $_POST[$name];
		}
		else if(strtolower($method) == "get" && isset($_GET[$name]))
		{
			$toCheck = $_GET[$name];
		}
			
		if($submitted)
		{
			if(isset($toCheck) && !$this instanceof CheckBox && !$this instanceof DropDown)
			{
				$this->setValue($toCheck);
			}
			else if(isset($toCheck) && $this instanceof CheckBox && in_array($this->getValue(), $toCheck))
			{
				$this->setChecked(true);
			}

			foreach($xml->error as $error)
			{
				$this->addError(new CustomError($error, $toCheck));
			}
		}
	}

	protected function setSubmitted($submitted)
	{
		if($submitted == false)
		{
			$this->submitted = false;
		}
		else
		{
			$this->submitted = true;
		}
	}

	protected function getSubmitted()
	{
		if(!isset($this->submitted))
		{
			$this->setSubmitted(false);
		}
		return $this->submitted;
	}

	protected function setDisabled($disabled)
	{
		if(!isset($disabled))
		{
			$disabled = false;
		}
		$disabled = (bool) ((string) $disabled);
		$this->disabled = $disabled;
	}

	protected function getDisabled()
	{
		if(!isset($this->disabled))
		{
			$this->setDisabled(false);
		}
		return $this->disabled;
	}

	protected function setPHPVariable($phpVariable)
	{
		$phpVariable = (string) $phpVariable;
		if(!isset($phpVariable))
		{
			$phpVariable = "";
		}
			
		$this->phpVariable = $phpVariable;
	}

	public function getPHPVariable()
	{
		if(!isset($this->phpVariable))
		{
			$this->setPHPVariable("");
		}
		return $this->phpVariable;
	}

	public function getKeyValueArray()
	{
		$key = $this->getPHPVariable();
		if(!isset($key) || $key == "")
		{
			$key = "Variable";
		}
		$value = $this->getValue();
			
		if(!isset($value))
		{
			$value = "";
		}
			
		$toReturn = array($key=>$value);
		return $toReturn;
	}

	public function getLabelClass()
	{
		if(!isset($this->labelClass))
		{
			$this->setLabelClass("");
		}
			
		return $this->labelClass;
	}

	public function setLabelClass($labelClass)
	{
		$this->labelClass = (string) $labelClass;
	}

	public function getSurroundingDivClass()
	{
		if(!isset($this->surroundingDivClass))
		{
			$this->setSurroundingDivClass("");
		}
			
		return $this->surroundingDivClass;
	}

	public function setSurroundingDivClass($divClass)
	{
		$this->surroundingDivClass = (string) $divClass;
	}

	public function getSurroundingSpanClass()
	{
		if(!isset($this->surroundingSpanClass))
		{
			$this->setSurroundingSpanClass("");
		}
			
		return $this->surroundingSpanClass;
	}

	public function setSurroundingSpanClass($spanClass)
	{
		$this->surroundingSpanClass = (string) $spanClass;
	}

	/*
	 * Gets the introduction data, such as if this element is hidden in a div or span
	 */
	protected function getIntroAndEnd()
	{
		$surroundingDivClass = $this->getSurroundingDivClass();
		$surroundingSpanClass = $this->getSurroundingSpanClass();

		$intro="";
		$end="";
		if($surroundingDivClass != "")
		{
			$intro .= '<div class="' . $surroundingDivClass . '">';
		}

		if($surroundingSpanClass != "")
		{
			$intro .= '<span class="' . $surroundingSpanClass . '">';
		}

		if($surroundingSpanClass != "")
		{
			$end .= '</span>';
		}
		if($surroundingDivClass != "")
		{
			$end .= '</div>';
		}

		return array("intro"=>$intro, "end"=>$end);
	}

	protected function getCSSStyle()
	{
		if(!isset($this->cssStyle))
		{
			$this->setCSSStyle("");
		}
		return $this->cssStyle;
	}

	protected function setCSSStyle($cssStyle)
	{
		$this->cssStyle = (string) $cssStyle;
	}
}
?>