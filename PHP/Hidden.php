<?php
/*
 * Copyright Michael Adsit 2012.
 */

include_once('Input.php');

class Hidden extends Input
{
	public function __construct($xml, $style, $method, $submitted)
	{
		parent::__construct($xml, $style, $method, $submitted);
	}

	public function getHTML()
	{
		$style = $this->getStyle();
		$toReturn = "";
		$toReturn .= $this->generateErrorString();
		$toReturn .= trim("<input type=\"hidden\" " . trim($this->getGenericElementString())) . "/ >";
		return $toReturn;
	}
}

?>