<?php
/*
 * Copyright Michael Adsit 2012.
 */

include_once('Input.php');

class UnorderedList extends Input
{
	protected $elements;

	public function __construct($xml, $style, $method, $submitted)
	{
		parent::__construct($xml, $style, $method, $submitted);
		$this->generateUniqueElements($xml);
	}

	public function getHTML()
	{
		$style = $this->getStyle();
		$toReturn = "";
		$toReturn .= $this->generateErrorString();
		$name = $this->getName();
			
		if($style == "table")
		{
			$toReturn .= "<tr><td>" . $this->getLabel() . "</td><td>";
		}
		else
		{
			$toReturn .= $this->getLabel();
		}
			
		$toReturn .= '<ul';
		if($this->getClass() != "")
		{
			$toReturn .= ' class="' . $this->getClass() . '"';
		}

		if($this->getCSSStyle() != "")
		{
			$toReturn .= ' style="' . $this->getCSSStyle() . '"';
		}

		$toReturn .= '>';
			
		foreach($this->getElements() as $element)
		{
			$toReturn .= '<li>';

			if(is_array($element))
			{
				foreach($element as $embedded)
				{
					$toReturn .= $embedded->getHTML();
				}
			}
			else
			{
				$toReturn .= $element->getHTML();
			}

			$toReturn .= '</li>';
		}
			
		$toReturn .= '</ul>';
			
		if($style == "table")
		{
			$toReturn .= "</td></tr>";
		}
			
		return $toReturn;
	}

	protected function generateUniqueElements()
	{
		$xml = $this->xml;
		if(isset($xml) && $xml instanceof SimpleXMLElement)
		{
			$this->setElements($xml);
		}
	}

	protected function setElements($xml)
	{
		$elements = array();
		if(isset($xml) && $xml instanceof SimpleXMLElement)
		{
			foreach($xml->input as $element)
			{
				if($element->type == "checkbox")
				{
					$input = $element->input;
					$element = new CheckBox($element, "none", $this->getMethod(), $this->getSubmitted());
					$name = $element->getName();
					if(substr($name, -2) != "[]")
					{
						$name = $name . "[]";
					}
					$element->setName($name);

					if(isset($input))
					{
						if($input->type == "unorderedlist")
						{
							$input = new UnorderedList($input, "none", $this->getMethod(), $this->getSubmitted());
							$element = array($element, $input);
						}
					}

					$elements[] = $element;
				}
				else if($element->type == "radio")
				{
					$input = $element->input;
					$element = new Radio($element, "none", $this->getMethod(), $this->getSubmitted());
					$name = $element->getName();
					if(substr($name, -2) != "[]")
					{
						$name = $name . "[]";
					}
					$element->setName($name);

					if(isset($input))
					{
						if($input->type == "unorderedlist")
						{
							$input = new UnorderedList($input, "none", $this->getMethod(), $this->getSubmitted());
							$element = array($element, $input);
						}
					}
					$elements[] = $element;
				}
				else if($element->type == "unorderedlist")
				{
					$element = new UnorderedList($element, "none", $this->getMethod(), $this->getSubmitted());
					$elements[] = $element;
				}
				else if($element->type == "label")
				{
					$input = $element->input;
					$element = new Label($element, "none", $this->getMethod(), $this->getSubmitted());

					if(isset($input))
					{
						if($input->type == "unorderedlist")
						{
							$input = new UnorderedList($input, "none", $this->getMethod(), $this->getSubmitted());
							$element = array($element, $input);
						}
					}
					$elements[] = $element;
				}
			}
		}
		$this->elements = $elements;
	}

	public function resetErrors()
	{
		foreach($this->getElements() as $element)
		{
			if(is_array($element))
			{
				foreach($element as $embedded)
				{
					$embedded->resetErrors();
				}
			}
			else
			{
				$element->resetErrors();
			}
		}
	}

	public function getElements()
	{
		if(!isset($this->elements))
		{
			$this->setElements($this->getXML());
		}
		return $this->elements;
	}

	public function getValue()
	{
		$toReturn = array();

		foreach($this->getElements() as $element)
		{
			if(is_array($element))
			{
				foreach($element as $embedded)
				{
					if(($embedded instanceof CheckBox || $embedded instanceof Radio) && $embedded->getChecked())
					{
						$toReturn[] = $embedded->getValue();
					}
					else if($embedded instanceof UnorderedList)
					{
						$toReturn = array_merge($toReturn, $embedded->getValue());
					}
				}
			}
			else if(($element instanceof CheckBox || $element instanceof Radio) && $element->getChecked())
			{
				$toReturn[] = $element->getValue();
			}
			else if($element instanceof UnorderedList)
			{
				$toReturn = array_merge($toReturn, $element->getValue());
			}
		}
		return $toReturn;
	}
}
?>