<?php
/*
 * Copyright Michael Adsit 2012.
 */

include_once('Option.php');

class OptionGroup
{
	private $label;
	private $labelClass;
	private $options;
	public static $groupNum = 0;

	public function __construct($xml)
	{
		OptionGroup::$groupNum++;
		$this->generateUniqueElements($xml);
	}

	public function getHTML($value)
	{
		$label = $this->getLabel();
		$labelClass = $this->getLabelClass();
		if($labelClass != "")
		{
			$label = '<span class="' . $labelClass .'">' . $label . '</span>';
		}
		$options = $this->getOptions();
			
		$toReturn = "<optgroup label=\"" . $label . "\">";
		foreach($options as $option)
		{
			$toReturn .= $option->getHTML($value);
		}
		$toReturn .= "</optgroup>";
			
		return $toReturn;
	}

	private function generateUniqueElements($xml)
	{
		if(isset($xml) && $xml instanceof SimpleXMLElement)
		{
			$this->setLabel($xml->label);
			$this->setOptions($xml);
		}
	}

	private function setLabel($label)
	{
		if(!isset($label) || (string) $label == "")
		{
			$label = "Group" . OptionsGroup::$groupNum;
		}
		$this->label = (string) $label;
	}

	private function getLabel()
	{
		if(!isset($this->label))
		{
			$this->setLabel("");
		}
		return $this->label;
	}

	private function setOptions($options)
	{
		$this->options = array();
		if(!isset($options))
		{
			$options = array();
		}

		if($options instanceof SimpleXMLElement)
		{
			foreach($options->children() AS $option)
			{
				$this->addOption(new Option($option));
			}
		}
		else if(is_array($options))
		{
			foreach($options AS $option)
			{
				$this->addOption($option);
			}
		}
		else
		{
			$this->options = array();
		}
	}

	private function addOption($option)
	{
		if(!isset($this->options) || !is_array($this->options))
		{
			$this->options = array();
		}

		if($option instanceof Option)
		{
			$this->options[] = $option;
		}
	}

	private function getOptions()
	{
		if(!isset($this->options))
		{
			$this->setOptions(array());
		}
		return $this->options;
	}
	
	private function getLabelClass()
	{
		if($this->labelClass == null)
		{
			$this->setLabelClass(null);
		}
		return $this->labelClass;
	}
	
	private function setLabelClass($newLabelClass)
	{
		if($newLabelClass == null)
		{
			$newLabelClass = "";
		}
		$this->labelClass = $newLabelClass;
	}
}
?>