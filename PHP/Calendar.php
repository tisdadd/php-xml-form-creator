<?php
/*
 * Copyright Michael Adsit 2012.
 */
include_once('Input.php');

class Calendar extends Input
{
	private $displayType;

	public final function validDisplayTypes()
	{
		return array("calendar", "datepicker", "calendarwithdatepicker", "datetimepicker", "calendarwithdatetimepicker", "timepicker");
	}

	public function __construct($xml, $style, $method, $submitted)
	{
		parent::__construct($xml, $style, $method, $submitted);
		$this->setDisplayType($xml->displaytype);
		$this->resetErrors();
	}

	public function getHTML()
	{
		$toReturn = $this->generateErrorString();
		$displayType = $this->getDisplayType();

		if($displayType == "calendar")
		{
			$toReturn .= $this->getHiddenHTML();
			$toReturn .= $this->getCalendarHTML();
		}
		else if($displayType == "datepicker")
		{
			$toReturn .= $this->getLabel();
			$toReturn .= $this->getDatePickerHTML();
		}
		else if($displayType == "calendarwithdatepicker")
		{
			$toReturn .= "<table><tr><td>";
			$toReturn .= $this->getCalendarHTML();
			$toReturn .= "</td><td>";
			$toReturn .= $this->getDatePickerHTML();
			$toReturn .= "</td></tr></table>";
		}
		else if($displayType == "datetimepicker")
		{
			$toReturn .= $this->getLabel();
			$toReturn .= $this->getDatePickerHTML();
			$toReturn .= $this->getTimePickerHTML();
		}
		else if($displayType == "calendarwithdatetimepicker")
		{
			$toReturn .= "<table><tr><td>";
			$toReturn .= $this->getCalendarHTML();
			$toReturn .= "</td><td>";
			$toReturn .= $this->getDatePickerHTML();
			$toReturn .= $this->getTimePickerHTML();
			$toReturn .= "</td></tr></table>";
		}
		else if($displayType == "timepicker")
		{
			$toReturn .= $this->getTimePickerHTML();
		}

		return $toReturn;
	}

	private function getTimePickerHTML()
	{
		$name = $this->getName();
		$date = $this->getValue();
		$stringTime = date('g/i/A', $date->getTimestamp());
		$timeArray = explode("/", $stringTime);
		$hour = $timeArray[0];
		$minute = $timeArray[1];
		$amPM = $timeArray[2];

		$toReturn = '';
		$toReturn .= "<select id=\"" . $name . "_Hour\" name=\"" . $name . "_Hour\">";
		for($i = 1; $i < 10; $i++)
		{
			$toReturn .= "<option value=\"" . $i . "\" ";
			if($hour == $i)
			{
				$toReturn .= "selected=\"selected\"";
			}
			$toReturn .= "\">0" . $i . "</option>";
		}
		for($i = 10; $i <= 12; $i++)
		{
			$toReturn .= "<option value=\"" . $i . "\" ";
			if($hour == $i)
			{
				$toReturn .= "selected=\"selected\"";
			}
			$toReturn .= "\">" . $i . "</option>";
		}
		$toReturn .= "</select>";
		$toReturn .= ":";

		$toReturn .= "<select id=\"" . $name . "_Minute\" name=\"" . $name . "_Minute\">";
		for($i = 0; $i < 10; $i++)
		{
			$toReturn .= "<option value=\"" . $i . "\" ";
			if($minute == $i)
			{
				$toReturn .= "selected=\"selected\"";
			}
			$toReturn .= "\">0" . $i . "</option>";
		}
		for($i = 10; $i <= 59; $i++)
		{
			$toReturn .= "<option value=\"" . $i . "\" ";
			if($minute == $i)
			{
				$toReturn .= "selected=\"selected\"";
			}
			$toReturn .= "\">" . $i . "</option>";
		}
		$toReturn .= "</select>";

		$toReturn .= "<select id=\"" . $name . "_AMPM\" name=\"" . $name . "_AMPM\">";
		$toReturn .= "<option value=\"AM\">AM</option>";
		$toReturn .= "<option value=\"PM\"";
		if($amPM == "PM")
		{
			$toReturn .= "selected=\"selected\"";
		}
		$toReturn .= ">PM</option>";
		$toReturn .= "</select>";

		return $toReturn;
	}

	private function getDatePickerHTML()
	{
		$date = $this->getValue();
		$stringDate = date('m/d/Y', $date->getTimeStamp());
		$dateArray = explode("/", $stringDate);
		$originalMonth = $dateArray[0];
		$originalDay = $dateArray[1];
		$originalYear = $dateArray[2];
		$date = new DateTime($dateArray[0] . "/1/" . $dateArray[2]);
		$date->add(new DateInterval('P1M'));
			
		$stringDate = date('m/d/Y', $date->getTimeStamp());
		$dateArray = explode("/", $stringDate);
			
		$date = new DateTime($dateArray[0] . "/1/" . $dateArray[2]);
			
			
		$date->sub(new DateInterval('P1D'));
		$stringDate = date('m/d/Y', $date->getTimeStamp());
		$dateArray = explode("/", $stringDate);
			
		$label = $this->getLabel();
		$name = $this->getName();
			
		$toReturn = $this->generateErrorString();
			
		$loopDate = new DateTime($dateArray[0] . "/1/" . $dateArray[2]);

		$toReturn .= "<select onchange=\"monthChange('" . $name . "', this[this.selectedIndex].value, '" . $this->getDisplayType() . "', '" . $this->getLabel() . "')\"  id=\"" . $name . "_Month\" name=\"" . $name . "_Month\">";
		for($i = 1; $i <= 12; $i++)
		{
			$toReturn .= '<option value="' . $i . '"';

			if($i == $originalMonth)
			{
				$toReturn .= ' selected="selected"';
			}

			$toReturn .= '>' . $i . '</option>';
		}
		$toReturn .= "</select>/";

		$toReturn .= "<select onchange=\"dayChange('" . $name . "', this[this.selectedIndex].value, '" . $this->getDisplayType() . "', '" . $this->getLabel() . "')\"; value=\"" . $dateArray[1] . "\"  id=\"" . $name . "_Day\" name=\"" . $name . "_Day\">";
		for($i = 1; $i <= $dateArray[1]; $i++)
		{
			$toReturn .= "<option value=\"" . $i . "\"";

			if($i == $originalDay)
			{
				$toReturn .= ' selected="selected"';
			}

			$toReturn .= ">" . $i . '</option>';
		}
		$toReturn .= "</select>/";

		$toReturn .= "<select onchange=\"yearChange('" . $name . "', this[this.selectedIndex].value, '" . $this->getDisplayType() . "', '" . $this->getLabel() . "')\";\" value=\"" . $dateArray[2] . "\" id=\"" . $name . "_Year\" name=\"" . $name . "_Year\">";
		for($i = ($dateArray[2] - 50); $i <= ($dateArray[2] + 50); $i++)
		{
			$toReturn .= "<option value=\"" . $i . "\"";
			if($i == $originalYear)
			{
				$toReturn .= ' selected="selected"';
			}
			$toReturn .= ">" . $i . '</option>';
		}
		$toReturn .= "</select>";

		return $toReturn;
	}



	private function getCalendarHTML()
	{
		$date = $this->getValue();
		$stringDate = date('m/d/Y', $date->getTimeStamp());
		$dateArray = explode("/", $stringDate);
		$originalDay = $dateArray[1];
		$date = new DateTime($dateArray[0] . "/1/" . $dateArray[2]);
		$date->add(new DateInterval('P1M'));

		$stringDate = date('m/d/Y', $date->getTimeStamp());
		$dateArray = explode("/", $stringDate);

		$date = new DateTime($dateArray[0] . "/1/" . $dateArray[2]);


		$date->sub(new DateInterval('P1D'));
		$stringDate = date('m/d/Y', $date->getTimeStamp());
		$dateArray = explode("/", $stringDate);

		$label = $this->getLabel();
		$name = $this->getName();

		$toReturn = $this->generateErrorString();
		if($toReturn <> "")
		{
			$toReturn .= "<br />";
		}
		$toReturn .= "<table class=\"calendar\"><caption>" . $label . "</caption><tr><td><a onclick=\"lastMonth('" . $name . "', '" . $this->getDisplayType() . "', '" . $this->getLabel() . "')\">&larr;</a></td><td class=\"month_year_display\" colspan=\"5\">";
		$toReturn .= date("F", $date->getTimeStamp()) . " " . $dateArray[2];
		$toReturn .= "</td><td><a onclick=\"nextMonth('" . $name . "', '" . $this->getDisplayType() . "', '" . $this->getLabel() . "')\">&rarr;</a></td></tr>";
		$toReturn .= "<tr><th>Su</th><th>Mo</th><th>Tu</th><th>We</th><th>Th</th><th>Fr</th><th>Sa</th>";
		$loopDate = new DateTime($dateArray[0] . "/1/" . $dateArray[2]);
		$dayOfWeek = date('w', $loopDate->getTimeStamp());
		if($dayOfWeek > 0)
		{
			$toReturn .= "<tr>";
			for($i = 0; $i < $dayOfWeek; $i++)
			{
				$toReturn .= "<td></td>";
			}
		}
			
		for($i = 1; $i <= $dateArray[1]; $i++)
		{
			$loopDate = new DateTime($dateArray[0] . "/" . $i . "/" . $dateArray[2]);
			$dayOfWeek = date("w", $loopDate->getTimeStamp());
			if($dayOfWeek == 0)
			{
				$toReturn .= "<tr>";
			}

			if($i != $originalDay)
			{
				$toReturn .= "<td><a onclick=\"dayChange('" . $name . "', '" . $i . "', '" . $this->getDisplayType() . "', '" . $this->getLabel() . "')\">" . $i . "</a></td>";
			}
			else
			{
				$toReturn .= "<td class=\"selectedDay\">" . $i . "</td>";
			}

			if($dayOfWeek == 6)
			{
				$toReturn .= "</tr>";
			}

		}
			
		if($dayOfWeek != 6)
		{
			for($i = $dayOfWeek; $i < 7; $i++)
			{
				$toReturn.= "<td></td>";
			}
			$toReturn .= "</tr>";
		}
			
		$toReturn .= "</table>";
		return $toReturn;
	}

	private function getHiddenHTML()
	{
		$date = $this->getValue();
		$stringDate = date('m/d/Y', $date->getTimeStamp());
		$dateArray = explode("/", $stringDate);
		$originalDay = $dateArray[1];
		$date = new DateTime($dateArray[0] . "/1/" . $dateArray[2]);
		$date->add(new DateInterval('P1M'));

		$stringDate = date('m/d/Y', $date->getTimeStamp());
		$dateArray = explode("/", $stringDate);

		$date = new DateTime($dateArray[0] . "/1/" . $dateArray[2]);


		$date->sub(new DateInterval('P1D'));
		$stringDate = date('m/d/Y', $date->getTimeStamp());
		$dateArray = explode("/", $stringDate);

		$label = $this->getLabel();
		$name = $this->getName();

		$toReturn = "";

		$toReturn .= "<input type=\"hidden\" name=\"" . $name . "_Year\" id=\"" . $name . "_Year\" value=\"" . $dateArray[2] . "\">";
		$toReturn .= "<input type=\"hidden\" name=\"" . $name . "_Month\"  id=\"" . $name . "_Month\" value=\"" . $dateArray[0] . "\">";
		$toReturn .= "<input type=\"hidden\" name=\"" . $name . "_Day\"  id=\"" . $name . "_Day\" value=\"" . $originalDay . "\">";

		return $toReturn;
	}

	public function setValue($value)
	{
		$date = $value;
		if(!isset($date))
		{
			$date = new DateTime();
		}
		if($date instanceof SimpleXMLElement && (string) $date != "")
		{
			$date = (string) $date;
		}
		if(is_string($date))
		{
			try
			{
				$originalDateArray = explode("/", $date);
				$date = new DateTime($date);
				$stringDate = date('m/d/Y/g/i/A', $date->getTimeStamp());
				$dateArray = explode("/", $stringDate);
					
				if($originalDateArray[0] != $dateArray[0])
				{ // day of month put it ahead one month...
					$date = new DateTime($dateArray[0] . "/1/" . $dateArray[2] . ' ' . $dateArray[3] . ':' . $dateArray[4] . ' ' . $dateArray[5]);
					$date->sub(new DateInterval('P1D'));
				}
			}
			catch (Exception $e)
			{
				$date = new DateTime();
			}
		}
		if(!($date instanceof DateTime))
		{
			$date = new DateTime();
		}
			
		$this->value = $date;
	}



	public function getValue()
	{
		if(!isset($this->value) || !($this->value instanceof DateTime))
		{
			$this->setValue();
		}
			
		return $this->value;
	}

	public function setDefaultValue($defaultValue)
	{
		$date = $defaultValue;
		if(!isset($date))
		{
			$date = new DateTime();
		}
		if($date instanceof SimpleXMLElement && (string) $date != "")
		{
			$date = (string) $date;
		}
		if(is_string($date))
		{
			try
			{
				$originalDateArray = explode("/", $date);
				$date = new DateTime($date);
				$stringDate = date('m/d/Y/g/i/A', $date->getTimeStamp());
				$dateArray = explode("/", $stringDate);
					
				if($originalDateArray[0] != $dateArray[0])
				{ // day of month put it ahead one month...
					$date = new DateTime($dateArray[0] . "/1/" . $dateArray[2] . ' ' . $dateArray[3] . ':' . $dateArray[4] . ' ' . $dateArray[5]);
					$date->sub(new DateInterval('P1D'));
				}
			}
			catch (Exception $e)
			{
				$date = new DateTime();
			}
		}
		if(!($date instanceof DateTime))
		{
			$date = new DateTime();
		}
			
		$this->defaultValue = $date;
	}

	public function resetErrors()
	{
		$xml = $this->xml;
		$method = $this->getMethod();
		$name = $this->getName();
		$submitted = $this->getSubmitted();

		$month = 1;
		$day = 2;
		$year = 3;
		$hour = 4;
		$minute = 5;
		$amPM = "AM";

		$timePickerTypeArray = array("datetimepicker", "calendarwithdatetimepicker", "timepicker");

		if(strtolower($method) == "post" && isset($_POST[$name . "_Month"]) && isset($_POST[$name . "_Day"]) && isset($_POST[$name . "_Year"]))
		{
			$month =$_POST[$name . "_Month"];
			$day = $_POST[$name . "_Day"];
			$year = $_POST[$name . "_Year"];
		}
		else if(strtolower($method) == "get" && isset($_GET[$name . "_Month"]) && isset($_GET[$name . "_Day"]) && isset($_GET[$name . "_Year"]))
		{
			$month =$_GET[$name . "_Month"];
			$day = $_GET[$name . "_Day"];
			$year = $_GET[$name . "_Year"];
		}

		if(in_array($this->getDisplayType(), $timePickerTypeArray))
		{
			if(isset($_GET[$name . "_Minute"]))
			{
				$minute =$_GET[$name . "_Minute"];
			}
			if(isset($_GET[$name . "_Hour"]))
			{
				$hour = $_GET[$name . "_Hour"];
			}
			if(isset($_GET[$name . "_AMPM"]))
			{
				$amPM = $_GET[$name . "_AMPM"];
			}

			if(isset($_POST[$name . "_Minute"]))
			{
				$minute =$_POST[$name . "_Minute"];
			}
			if(isset($_POST[$name . "_Hour"]))
			{
				$hour = $_POST[$name . "_Hour"];
			}
			if(isset($_POST[$name . "_AMPM"]))
			{
				$amPM = $_POST[$name . "_AMPM"];
			}
		}

		if(in_array($this->getDisplayType(), $timePickerTypeArray))
		{
			$toCheck=$month . "/" . $day . "/" . $year . " " . $hour . ":" . substr("00" . $minute, -2) . " " . $amPM;
		}
		else
		{
			$toCheck=$month . "/" . $day . "/" . $year;
		}

		$date = $this->getValue();

		if($submitted)
		{
			if(isset($toCheck))
			{
				$this->setValue($toCheck);
			}
			$value = $this->getValue();
			$this->addError(new CustomError(new SimpleXMLElement("<error><condition>date</condition><message>Somehow this isn't a date.</message></error>"), $toCheck));
			foreach($xml->error as $error)
			{
				$this->addError(new CustomError($xml->error, $toCheck));
			}
		}
	}

	public function setDisplayType($displayType)
	{
		$displayType = strtolower((string) $displayType);
		$validTypes = $this->validDisplayTypes();
		if(in_array($displayType,$validTypes))
		{
			$this->displayType = $displayType;
		}
		else
		{
			$this->displayType = $validTypes[0];
		}
	}

	public function getDisplayType()
	{
		if(!isset($this->displayType))
		{
			$this->setDisplayType("Calendar");
		}
			
		return $this->displayType;
	}
}
?>